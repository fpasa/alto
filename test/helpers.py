from pathlib import Path

from alto.builder import Builder
from alto.nodes import Module


def get_builtin_deps() -> dict[str, Module]:
    """Hacky method that exposes the builtins module to the tests."""
    builder = Builder(Path(), Path() / "exe", [])
    builder._prepare_build_dir()
    builtins_path = builder._resolve_module("std/builtins")
    builder._compile_module_to_ast_with_deps(builtins_path, "-builtins-")
    return builder.modules
