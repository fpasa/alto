import shutil
from pathlib import Path

import pytest

from alto.builder import _read_entrypoint, build_source

TEST_DIR = Path(__file__).parent.parent
LLVM_IR_DIR = TEST_DIR / "llvm_ir"
TEST_BUILDER_DIR = TEST_DIR / "data" / "builder"
SCRIPT_PATH = TEST_BUILDER_DIR / "script.at"
MODULE_PATH = TEST_BUILDER_DIR / "module"
MODULES_TEST_PATH = TEST_BUILDER_DIR / "modules"
MODULES_BIN_PATH = MODULES_TEST_PATH / "bin"
FILE1_PATH = TEST_BUILDER_DIR / "module" / "file1.at"
FILE2_PATH = TEST_BUILDER_DIR / "module" / "file2.at"


def _copy_llvm_ir(build_dir: Path) -> None:
    if LLVM_IR_DIR.exists():
        shutil.rmtree(LLVM_IR_DIR)
    LLVM_IR_DIR.mkdir()

    for ir_file in build_dir.glob("*.ll"):
        shutil.copy(ir_file, LLVM_IR_DIR / ir_file.name)


@pytest.mark.parametrize(
    "entrypoint, expected",
    [
        (SCRIPT_PATH, f"{SCRIPT_PATH.read_text()}\n"),
        (
            MODULE_PATH,
            f"# file: file1.at\n{FILE1_PATH.read_text()}\n\n"
            f"# file: file2.at\n{FILE2_PATH.read_text()}\n",
        ),
    ],
    ids=["script", "module"],
)
def test_read_entrypoint(entrypoint: Path, expected: str) -> None:
    code = _read_entrypoint(entrypoint)
    assert code == expected


@pytest.mark.parametrize(
    "entrypoint, search_path",
    [
        (SCRIPT_PATH, []),
        (MODULE_PATH, []),
        (MODULES_BIN_PATH, [MODULES_TEST_PATH]),
    ],
    ids=["script", "module", "module_linking"],
)
def test_build_source(
    entrypoint: Path,
    search_path: list[Path],
    tmp_path: Path,
    monkeypatch: pytest.MonkeyPatch,
) -> None:
    """This tests in cases of script, module and dependent modules that
    the compiler manages to compile and link the program properly.
    """
    try:
        monkeypatch.chdir(tmp_path)

        build_source(entrypoint, tmp_path / "exe", search_path)

        exe_path = tmp_path / "exe"
        assert exe_path.exists()
    except:
        # Copy LLVM to llvm_ir folder for manual
        _copy_llvm_ir(tmp_path / "build")
        raise
