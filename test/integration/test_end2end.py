"""Tests in this module compile and run a program,
checking that the program behaves as expected.
"""
from pathlib import Path

import pytest

from alto.__main__ import _run

TEST_DIR = Path(__file__).parent.parent
TEST_END2END_DIR = TEST_DIR / "data" / "end2end"
TEST_CASES = [
    ("call_args.at", 3),
    ("struct.at", 2),
    ("math.at", 5),
    ("if_bool_op.at", 22),
    # fibonacci tests if untyped literals compared with
    # comparison operators are converted to the right type.
    ("fib.at", 55),
    ("generics.at", 123),
    ("assignment.at", 9),
    ("assignment_struct.at", 21),
    ("array_index.at", 6),
    ("for_condition.at", 17),
    ("method.at", 150),
    ("for_loop.at", 2),
    ("nested_struct.at", 41),
    ("string_literals.at", 8),
]


@pytest.mark.parametrize(
    "file_name, expected_return_code",
    TEST_CASES,
    ids=[path[:-3] for path, _ in TEST_CASES],
)
def test_e2e(file_name: str, expected_return_code: int) -> None:
    with pytest.raises(SystemExit) as err:
        _run(TEST_END2END_DIR / file_name, [])

    assert err.value.code == expected_return_code
