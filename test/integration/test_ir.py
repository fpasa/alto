from pathlib import Path

import pytest

from alto.builder import compile_to_ast
from test.helpers import get_builtin_deps

TEST_DIR = Path(__file__).parent.parent
TEST_IR_DIR = TEST_DIR / "data" / "ir"

IR_TEST_CASES = [file.stem for file in TEST_IR_DIR.glob("*.at")]


@pytest.mark.parametrize(
    "code, expected_ir",
    [
        (
            (TEST_IR_DIR / f"{f}.at").read_text(),
            (TEST_IR_DIR / f"{f}.ll").read_text(),
        )
        for f in IR_TEST_CASES
    ],
    ids=IR_TEST_CASES,
)
def test_ir(code: str, expected_ir: Path, tmp_path: Path) -> None:
    ast = compile_to_ast("-main-", code, get_builtin_deps())
    actual_ir = ast.get_llvm_ir()
    try:
        assert actual_ir == expected_ir
    except AssertionError:
        print(
            f"\n\n# Actual\n# ---------\n{actual_ir}\n\n"
            f"# Expected\n# ---------\n{expected_ir}\n\n"
        )
        raise
