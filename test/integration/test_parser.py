from pathlib import Path

import pytest

from alto.parser import parse

TEST_DIR = Path(__file__).parent.parent
GOOD_TEST_FILES = list((TEST_DIR / "data" / "parser" / "good").glob("*.at"))
BAD_TEST_FILES = list((TEST_DIR / "data" / "parser" / "bad").glob("*.at"))


@pytest.mark.parametrize(
    "code",
    [f.read_text() for f in GOOD_TEST_FILES],
    ids=[f.stem for f in GOOD_TEST_FILES],
)
def test_parser(code: str) -> None:
    parse(code)


@pytest.mark.parametrize(
    "code",
    [f.read_text() for f in BAD_TEST_FILES],
    ids=[f.stem for f in BAD_TEST_FILES],
)
def test_parser_error(code: str) -> None:
    with pytest.raises(Exception):
        parse(code)
