from textwrap import dedent

import pytest

from alto.builder import compile_to_ast
from alto.nodes import (
    Module,
    StructFieldAccessHelper,
    Not,
    Reference,
)
from alto.nodes.expressions import (
    IntLiteral,
    BoolLiteral,
    Call,
)
from alto.nodes.operators import Neg, And, GTE, Add, Sub, Mul, Div, Rem
from alto.nodes.struct_expr import StructConstruction, StructFieldReadOperation
from alto.optimize import optimize_ast
from alto.tree import _add_builtins, Context
from test.helpers import get_builtin_deps


def _module(name: str = "-main-") -> Module:
    module = Module("-main-")
    context = Context(module)
    _add_builtins(module, context, get_builtin_deps())
    return module


def _minimal_main_ast() -> Module:
    module = _module()

    main_fun = module.add_function_def("main", "i32")
    main_fun.add_return(IntLiteral("0"))

    return module


def _add_ast() -> Module:
    module = _module()

    main_fun = module.add_function_def("main", "i32")
    main_fun.add_return(Add(IntLiteral("1"), IntLiteral("2")))

    return module


def _fun_args_ast() -> Module:
    module = _module()

    add_fun = module.add_function_def("add", "i32", {"a": "i32", "b": "i32"})
    add_fun.add_return(Add(add_fun.get_arg("a"), add_fun.get_arg("b")))

    main_fun = module.add_function_def("main", "i32")
    main_fun.add_return(Call(add_fun, [IntLiteral("1"), IntLiteral("2")]))

    return module


def _struct_ast() -> Module:
    module = _module()

    test_struct = module.add_struct("Test", {"id": "u64", "age": "u8"})

    read_it_fun = module.add_function_def(
        "read_it", "i32", {"test": Reference(test_struct)}
    )
    read_it_fun.add_return(
        StructFieldReadOperation(
            StructFieldAccessHelper(read_it_fun.get_arg("test"), test_struct, "age")
        )
    )

    main_fun = module.add_function_def("main", "i32")
    param = StructConstruction(test_struct, [IntLiteral("1"), IntLiteral("2")])
    main_fun.add_return(Call(read_it_fun, [param]))

    return module


def _math_precedence_ast() -> Module:
    module = _module()

    main_fun = module.add_function_def("main", "i32")
    main_fun.add_return(
        # 1 + (4 - 3 * 2) + 5 / 2 - -((27 / 3) % 5)
        Add(
            IntLiteral("1"),
            Add(
                Sub(IntLiteral("4"), Mul(IntLiteral("3"), IntLiteral("2"))),
                Sub(
                    Div(IntLiteral("5"), IntLiteral("2")),
                    Neg(
                        Rem(
                            Div(IntLiteral("27"), IntLiteral("3")),
                            IntLiteral("5"),
                        )
                    ),
                ),
            ),
        )
    )

    return module


def _if_operator_ast() -> Module:
    module = _module()

    main_fun = module.add_function_def("main", "u32")
    if_stmt = main_fun.add_if(
        And(
            GTE(
                Add(IntLiteral("1"), Mul(IntLiteral("2"), IntLiteral("6"))),
                Sub(IntLiteral("5"), Div(IntLiteral("3"), IntLiteral("2"))),
            ),
            Not(BoolLiteral("true")),
        )
    )
    if_stmt.if_.add_return(IntLiteral("1"))

    else_if_stmt = if_stmt.add_else_if(BoolLiteral("false"))
    else_if_stmt.if_.add_return(IntLiteral("2"))

    else_if_stmt.add_else()
    else_if_stmt.else_.add_return(IntLiteral("3"))

    main_fun.add_return(IntLiteral("4"))

    # Optimization removes some after blocks here
    return optimize_ast(module)


def _generics_ast() -> Module:
    module = _module()

    holder_struct = module.add_struct("Holder", {"content": "T"}, ["T"])
    spec = holder_struct.specialize({"T": "u32"})

    main_fun = module.add_function_def("main", "u32")
    main_fun.add_return(
        StructFieldReadOperation(
            StructFieldAccessHelper(
                StructConstruction(spec, [IntLiteral("123")]),
                spec,
                "content",
            )
        )
    )

    return module


@pytest.mark.parametrize(
    "code, module",
    [
        (
            """
            fun main() i32:
               return 0
            """,
            _minimal_main_ast(),
        ),
        (
            """
            fun main() i32:
               return 1 + 2
            """,
            _add_ast(),
        ),
        (
            """
            fun add(a i32, b i32) i32:
               return a + b
               
            fun main() i32:
               return add(1, 2)
            """,
            _fun_args_ast(),
        ),
        (
            """
            struct Test:
               id u64
               age u8
            
            fun main() i32:
               return read_it(Test(1, 2))
            
            fun read_it(test Test) i32:
               return test.age
            """,
            _struct_ast(),
        ),
        (
            """
            fun main() i32:
                return 1 + (4 - 3 * 2) + 5 / 2 - -((27 / 3) % 5)
            """,
            _math_precedence_ast(),
        ),
        (
            """
            fun main() u32:
                if 1 + 2*6 >= 5 - 3/2 and not true:
                    return 1
                else if false:
                    return 2
                else:
                    return 3
                  
                return 4
            """,
            _if_operator_ast(),
        ),
        (
            """
            struct Holder<T>:
               content T
            
            fun main() u32:
               return Holder!<u32>(123).content
            """,
            _generics_ast(),
        ),
    ],
    ids=[
        "minimal_main",
        "add",
        "fun_args",
        "struct",
        "math_precedence",
        "if_operator_bool",
        "generics",
    ],
)
def test_compile_to_ast(code: str, module: Module) -> None:
    code = dedent(code).strip()
    ast = compile_to_ast("-main-", code)
    assert ast == module


@pytest.mark.parametrize(
    "code",
    [
        """
        fun main() i32:
           let var = 1
           let var = 2
        """,
        """
        fun main(var i32) i32:
           let var = 2
        """,
        """
        fun main() i32:
           let var = 1
           var = 2
        """,
        """
        fun var() u32:
            return 0
        
        fun main() i32:
           let var = 1
        """,
        """
        fun main() i32:
           var = 1
        """,
    ],
    ids=[
        "redeclaration",
        "redeclaration_arg",
        "assignment_to_immutable",
        "redeclaration_outer_symbol",
        "no_declaration",
    ],
)
def test_ast_errors(code: str) -> None:
    with pytest.raises(RuntimeError):
        code = dedent(code).strip()
        compile_to_ast("-main-", code)
