%-builtins-.-Array_u8- = type { i64 }
%-builtins-.-ArrayIterator_u8- = type { ptr, i64 }
%-builtins-.str = type { %-builtins-.-Array_u8- }

declare i64 @-builtins-.Array.-len_u8-(ptr)
declare ptr @-builtins-.Array.-iter_u8-(ptr)

declare i8 @-builtins-.ArrayIterator.-next_u8-(ptr)

declare i64 @-builtins-.str.byte_len(ptr)

@string-0 = constant { i64, [9 x i8] } { i64 8, [9 x i8] c"my value\00" }

declare ptr @GC_malloc(i64)

define i64 @main() {
start:
  %0 = call i64 @-builtins-.str.byte_len(ptr @string-0)
  ret i64 %0
}