%-main-.Measurement = type { i32 }
%-main-.ReportEmbed = type { i64, %-main-.Measurement }
%-main-.ReportRef = type { i64, ptr }

declare ptr @GC_malloc(i64)

define i32 @main() {
start:
  %0 = getelementptr %-main-.ReportEmbed, ptr null, i32 1
  %1 = call ptr @GC_malloc(ptr %0)
  %2 = getelementptr inbounds %-main-.ReportEmbed, ptr %1, i32 0, i32 0
  store i64 123, ptr %2
  %3 = getelementptr inbounds %-main-.ReportEmbed, ptr %1, i32 0, i32 1, i32 0
  store i32 37, ptr %3
  %report_embed = alloca ptr
  store ptr %1, ptr %report_embed
  %4 = getelementptr %-main-.ReportRef, ptr null, i32 1
  %5 = call ptr @GC_malloc(ptr %4)
  %6 = getelementptr inbounds %-main-.ReportRef, ptr %5, i32 0, i32 0
  store i64 123, ptr %6
  %7 = getelementptr %-main-.Measurement, ptr null, i32 1
  %8 = call ptr @GC_malloc(ptr %7)
  %9 = getelementptr inbounds %-main-.Measurement, ptr %8, i32 0, i32 0
  store i32 33, ptr %9
  %10 = getelementptr inbounds %-main-.ReportRef, ptr %5, i32 0, i32 1
  store ptr %8, ptr %10
  %report_ref = alloca ptr
  store ptr %5, ptr %report_ref
  %11 = load ptr, ptr %report_embed
  %12 = getelementptr inbounds %-main-.ReportEmbed, ptr %11, i32 0, i32 1
  %measurement_embed = alloca ptr
  store ptr %12, ptr %measurement_embed
  %13 = load ptr, ptr %report_embed
  %14 = getelementptr inbounds %-main-.ReportEmbed, ptr %13, i32 0, i32 1
  %15 = getelementptr inbounds %-main-.Measurement, ptr %14, i32 0, i32 0
  %16 = load i32, ptr %15
  %17 = load ptr, ptr %measurement_embed
  %18 = getelementptr inbounds %-main-.Measurement, ptr %17, i32 0, i32 0
  %19 = load i32, ptr %18
  %20 = load ptr, ptr %report_ref
  %21 = getelementptr inbounds %-main-.ReportRef, ptr %20, i32 0, i32 1
  %22 = load ptr, ptr %21
  %23 = getelementptr inbounds %-main-.Measurement, ptr %22, i32 0, i32 0
  %24 = load i32, ptr %23
  %25 = sub i32 %19, %24
  %26 = add i32 %16, %25
  ret i32 %26
}