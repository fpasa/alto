%-main-.Person = type { i32 }

declare ptr @GC_malloc(i64)

define i32 @main() {
start:
  %0 = getelementptr %-main-.Person, ptr null, i32 1
  %1 = call ptr @GC_malloc(ptr %0)
  %2 = getelementptr inbounds %-main-.Person, ptr %1, i32 0, i32 0
  store i32 18, ptr %2
  %john = alloca ptr
  store ptr %1, ptr %john
  %3 = load ptr, ptr %john
  %4 = getelementptr inbounds %-main-.Person, ptr %3, i32 0, i32 0
  store i32 21, ptr %4
  %5 = load ptr, ptr %john
  %6 = getelementptr inbounds %-main-.Person, ptr %5, i32 0, i32 0
  %7 = load i32, ptr %6
  ret i32 %7
}