%-builtins-.-ArrayIterator_u8- = type { ptr, i64 }
%-builtins-.-ArrayIterator_u32- = type { ptr, i64 }
%-builtins-.-Array_u8- = type { i64 }
%-builtins-.-Array_u32- = type { i64 }

declare i8 @-builtins-.ArrayIterator.-next_u8-(ptr)

declare i32 @-builtins-.ArrayIterator.-next_u32-(ptr)

declare i64 @-builtins-.Array.-len_u8-(ptr)
declare ptr @-builtins-.Array.-iter_u8-(ptr)

declare i64 @-builtins-.Array.-len_u32-(ptr)
declare ptr @-builtins-.Array.-iter_u32-(ptr)

declare ptr @GC_malloc(i64)

define i32 @main() {
start:
  %0 = getelementptr {%-builtins-.-Array_u32-, [0 x i32]}, ptr null, i32 0, i32 1, i64 5
  %1 = call ptr @GC_malloc(ptr %0)
  %2 = getelementptr inbounds %-builtins-.-Array_u32-, ptr %1, i32 0, i32 0
  store i64 5, ptr %2
  %array = alloca ptr
  store ptr %1, ptr %array
  %3 = load ptr, ptr %array
  %4 = getelementptr {%-builtins-.-Array_u32-, [0 x i32]}, ptr %3, i32 0, i32 1, i64 0
  store i32 5, ptr %4
  %5 = load ptr, ptr %array
  %6 = getelementptr {%-builtins-.-Array_u32-, [0 x i32]}, ptr %5, i32 0, i32 1, i64 1
  store i32 4, ptr %6
  %7 = load ptr, ptr %array
  %8 = getelementptr {%-builtins-.-Array_u32-, [0 x i32]}, ptr %7, i32 0, i32 1, i64 2
  store i32 3, ptr %8
  %9 = load ptr, ptr %array
  %10 = getelementptr {%-builtins-.-Array_u32-, [0 x i32]}, ptr %9, i32 0, i32 1, i64 3
  store i32 2, ptr %10
  %11 = load ptr, ptr %array
  %12 = getelementptr {%-builtins-.-Array_u32-, [0 x i32]}, ptr %11, i32 0, i32 1, i64 4
  store i32 1, ptr %12
  %count = alloca i64
  store i64 0, ptr %count
  %13 = load ptr, ptr %array
  %14 = call ptr @-builtins-.Array.-iter_u32-(ptr %13)
  %-iterable_1- = alloca ptr
  store ptr %14, ptr %-iterable_1-
  br label %for0

for0:
  %15 = load ptr, ptr %-iterable_1-
  %16 = call i32 @-builtins-.ArrayIterator.-next_u32-(ptr %15)
  %val = alloca i32
  store i32 %16, ptr %val
  %17 = load i64, ptr %count
  %18 = icmp eq i64 %17, 3
  br i1 %18, label %if1, label %after1

if1:
  %19 = load i32, ptr %val
  ret i32 %19

after1:
  %20 = load i64, ptr %count
  %21 = add i64 %20, 1
  store i64 %21, ptr %count
  br label %for0
}