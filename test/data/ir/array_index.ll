%-builtins-.-ArrayIterator_u8- = type { ptr, i64 }
%-builtins-.-ArrayIterator_i32- = type { ptr, i64 }
%-builtins-.-Array_u8- = type { i64 }
%-builtins-.-Array_i32- = type { i64 }

declare i8 @-builtins-.ArrayIterator.-next_u8-(ptr)

declare i32 @-builtins-.ArrayIterator.-next_i32-(ptr)

declare i64 @-builtins-.Array.-len_u8-(ptr)
declare ptr @-builtins-.Array.-iter_u8-(ptr)

declare i64 @-builtins-.Array.-len_i32-(ptr)
declare ptr @-builtins-.Array.-iter_i32-(ptr)

declare ptr @GC_malloc(i64)

define i32 @main() {
start:
  %0 = getelementptr {%-builtins-.-Array_i32-, [0 x i32]}, ptr null, i32 0, i32 1, i64 5
  %1 = call ptr @GC_malloc(ptr %0)
  %2 = getelementptr inbounds %-builtins-.-Array_i32-, ptr %1, i32 0, i32 0
  store i64 5, ptr %2
  %array = alloca ptr
  store ptr %1, ptr %array
  %3 = load ptr, ptr %array
  %4 = getelementptr {%-builtins-.-Array_i32-, [0 x i32]}, ptr %3, i32 0, i32 1, i64 1
  store i32 7, ptr %4
  %5 = load ptr, ptr %array
  %6 = getelementptr {%-builtins-.-Array_i32-, [0 x i32]}, ptr %5, i32 0, i32 1, i64 2
  store i32 13, ptr %6
  %7 = load ptr, ptr %array
  %8 = getelementptr {%-builtins-.-Array_i32-, [0 x i32]}, ptr %7, i32 0, i32 1, i64 4
  %9 = load i32, ptr %8
  %10 = load ptr, ptr %array
  %11 = getelementptr {%-builtins-.-Array_i32-, [0 x i32]}, ptr %10, i32 0, i32 1, i64 2
  %12 = load i32, ptr %11
  %13 = load ptr, ptr %array
  %14 = getelementptr {%-builtins-.-Array_i32-, [0 x i32]}, ptr %13, i32 0, i32 1, i64 1
  %15 = load i32, ptr %14
  %16 = sub i32 %12, %15
  %17 = add i32 %9, %16
  ret i32 %17
}