%-main-.Test = type { i64, i32 }

declare ptr @GC_malloc(i64)

define i32 @main() {
start:
  %0 = getelementptr %-main-.Test, ptr null, i32 1
  %1 = call ptr @GC_malloc(ptr %0)
  %2 = getelementptr inbounds %-main-.Test, ptr %1, i32 0, i32 0
  store i64 1, ptr %2
  %3 = getelementptr inbounds %-main-.Test, ptr %1, i32 0, i32 1
  store i32 2, ptr %3
  %4 = call i32 @-main-.read_it(ptr %1)
  ret i32 %4
}

define i32 @-main-.read_it(ptr %-arg_test-) {
start:
  %0 = getelementptr inbounds %-main-.Test, ptr %-arg_test-, i32 0, i32 1
  %1 = load i32, ptr %0
  ret i32 %1
}