declare ptr @GC_malloc(i64)

define i32 @main() {
start:
  %0 = call i32 @-main-.add(i32 1, i32 2)
  ret i32 %0
}

define i32 @-main-.add(i32 %-arg_a-, i32 %-arg_b-) {
start:
  %0 = add i32 %-arg_a-, %-arg_b-
  ret i32 %0
}