declare ptr @GC_malloc(i64)

define i32 @main() {
start:
  %0 = mul i64 2, 6
  %1 = add i64 1, %0
  %2 = sdiv i64 3, 2
  %3 = sub i64 5, %2
  %4 = icmp sle i64 %1, %3
  %5 = sub i1 1, 1
  %6 = and i1 %4, %5
  br i1 %6, label %if0, label %else0

if0:
  ret i32 1

else0:
  br i1 0, label %if1, label %else1

if1:
  %7 = icmp slt i64 1, 2
  br i1 %7, label %if2, label %after2

if2:
  ret i32 7

after2:
  ret i32 4

else1:
  ret i32 22

after0:
  ret i32 33
}