declare ptr @GC_malloc(i64)

define i32 @main() {
start:
  %0 = mul i32 3, 2
  %1 = sub i32 4, %0
  %2 = sdiv i32 5, 2
  %3 = sdiv i32 27, 3
  %4 = srem i32 %3, 5
  %5 = sub i32 0, %4
  %6 = sub i32 %2, %5
  %7 = add i32 %1, %6
  %8 = add i32 1, %7
  ret i32 %8
}