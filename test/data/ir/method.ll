%-main-.Employee = type { i64, i32 }

define i32 @-main-.Employee.monthly_salary(ptr %-arg_self-) {
start:
  %0 = getelementptr inbounds %-main-.Employee, ptr %-arg_self-, i32 0, i32 1
  %1 = load i32, ptr %0
  %2 = udiv i32 %1, 12
  ret i32 %2
}

declare ptr @GC_malloc(i64)

define i32 @main() {
start:
  %0 = getelementptr %-main-.Employee, ptr null, i32 1
  %1 = call ptr @GC_malloc(ptr %0)
  %2 = getelementptr inbounds %-main-.Employee, ptr %1, i32 0, i32 0
  store i64 123, ptr %2
  %3 = getelementptr inbounds %-main-.Employee, ptr %1, i32 0, i32 1
  store i32 1800, ptr %3
  %maria = alloca ptr
  store ptr %1, ptr %maria
  %4 = load ptr, ptr %maria
  %5 = call i32 @-main-.Employee.monthly_salary(ptr %4)
  ret i32 %5
}