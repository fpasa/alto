declare ptr @GC_malloc(i64)

define i32 @main() {
start:
  %num = alloca i32
  store i32 1, ptr %num
  %0 = load i32, ptr %num
  %1 = add i32 %0, 2
  %other_num = alloca i32
  store i32 %1, ptr %other_num
  %2 = load i32, ptr %other_num
  store i32 %2, ptr %other_num
  %3 = load i32, ptr %other_num
  %4 = mul i32 %3, 3
  store i32 %4, ptr %other_num
  %5 = load i32, ptr %other_num
  ret i32 %5
}