%-main-.-Holder_u32- = type { i32 }

declare ptr @GC_malloc(i64)

define i32 @-main-.-content_u32-(ptr %-arg_holder-) {
start:
  %0 = getelementptr inbounds %-main-.-Holder_u32-, ptr %-arg_holder-, i32 0, i32 0
  %1 = load i32, ptr %0
  ret i32 %1
}

define i32 @main() {
start:
  %0 = getelementptr %-main-.-Holder_u32-, ptr null, i32 1
  %1 = call ptr @GC_malloc(ptr %0)
  %2 = getelementptr inbounds %-main-.-Holder_u32-, ptr %1, i32 0, i32 0
  store i32 123, ptr %2
  %3 = call i32 @-main-.-content_u32-(ptr %1)
  ret i32 %3
}