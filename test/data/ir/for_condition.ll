declare ptr @GC_malloc(i64)

define i32 @main() {
start:
  %count = alloca i32
  store i32 0, ptr %count
  br label %for0

for0:
  %0 = load i32, ptr %count
  %1 = icmp ult i32 %0, 17
  %2 = sub i1 1, %1
  br i1 %2, label %if1, label %after1

if1:
  br label %after0

after1:
  %3 = load i32, ptr %count
  %4 = add i32 %3, 1
  store i32 %4, ptr %count
  br label %for0

after0:
  %5 = load i32, ptr %count
  ret i32 %5
}