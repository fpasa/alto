from alto.tree import order_dependencies


def test_order_dependencies() -> None:
    dep_map = {
        None: ["Person", "Appointment"],
        "Person": ["Department", "Ticket"],
        "Department": ["Company"],
    }
    result = list(order_dependencies(dep_map))
    assert result == ["Person", "Appointment", "Department", "Ticket", "Company"]
