# Alto Roadmap

# Other

- Enums
- Array literals
- Array Views
- "in" operator
- "**" operator
- Type inference for generics
- Generic parameters for methods

# Draft 0.7.0 - Filler Features

- Number type casts
- Install script

# Draft 0.6.0 - Traits

- Traits
- Trait restrictions on generic parameters
- Ranges (iteration only)

# Draft 0.5.0 - Last Mile to Useful

- std/builtin (list)
- List literals
- std/fs (filesystem utils)
- std/file (file utils)

# WIP 0.4.0 - Strings & std/builtin

- Reference types & struct embedding
- Size integer types
- Strings
- Fixes to struct construction and dependencies.
- Deduplication of generic struct variant implementation.
- Constants
- std/builtin (print)
- Pub visibility for structs and functions
- String interpolation
- Void functions
- C interop pointer types?

# 0.3.0 - Arrays & Loops (21 Nov 2022)

- Generic structs
- Generic functions
- Variables
- Arrays
- Array Indexing
- For loops
- Struct methods

# 0.2.0 - More Types, Variables, Control Flow (23 Oct 2022)

- Better declarations
- If statements
- Comparison operators
- Logical operators
- Boolean type
- Float literals

# 0.1.0 - Modules, Functions, Structs and Basic Math (13 Oct 2022)

- Parser & Compiler infrastructure
- Ability to run scripts
- Module system
- Functions
- Integer types & literals
- Float types
- Basic Math
- Garbage Collection & Heap Allocation
- Basic Structs
