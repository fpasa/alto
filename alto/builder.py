import subprocess
from pathlib import Path
from typing import Optional

from lark import Tree

from alto.optimize import optimize_ast
from alto.parser import parse
from alto.tree import build_module_ast, is_of_type
from alto.nodes import Module

DEFAULT_SEARCH_PATH = Path(__file__).parent / "search_path"
_FILE_EXT = ".at"
_FILE_SEP = "\n"


def build_source(
    entrypoint: Path,
    executable_path: Path,
    search_paths: list[Path],
) -> None:
    builder = Builder(entrypoint, executable_path, search_paths)
    builder.build()


class Builder:
    def __init__(
        self,
        entrypoint: Path,
        executable_path: Path,
        search_paths: list[Path],
    ):
        self.entrypoint = entrypoint
        self.executable_path = executable_path
        self.search_paths = {*search_paths, Path(), DEFAULT_SEARCH_PATH}
        self.build_dir = Path("build")
        self.modules: dict[str, Module] = {}
        self.object_paths: list[Path] = []

    def _prepare_build_dir(self) -> None:
        self.build_dir.mkdir(exist_ok=True)

    def build(self) -> None:
        self._prepare_build_dir()

        # First compile all modules to AST.
        # This needs to be done first as there might be dependencies
        # among modules, such as a module using a generic from
        # another module. If we don't to build the full AST first,
        # the module defining the generic can miss variants needed
        # for other modules and fail at link time.
        #
        # Having the generic variants implemented in the defining
        # module reduce duplication (and therefore code size),
        # because a variant can be reused by different modules.

        # Always build std/builtins module
        # TODO: cache this!
        builtins_path = self._resolve_module("std/builtins")
        self._compile_module_to_ast_with_deps(builtins_path, "-builtins-")

        # The module that is build into an executable is called -main-
        self._compile_module_to_ast_with_deps(self.entrypoint, "-main-")

        # When the AST is complete, we can build object files
        self._generate_objects()

        _link(self.object_paths, self.executable_path)

    def _compile_module_to_ast_with_deps(
        self, module_path: Path, module_name: str
    ) -> None:
        # FIXME: This code currently does not support dependency loops.
        code = _read_entrypoint(module_path)
        tree = parse(code)

        # The file or module might depend on other modules.
        # Build dependencies before building the current module.
        for node in tree.children:
            if is_of_type(node, "import"):
                dep_name = node.children[0].value

                # Do not build module if it was already built
                if dep_name not in self.modules:
                    module_path = self._resolve_module(dep_name)
                    self._compile_module_to_ast_with_deps(module_path, dep_name)

        self._compile_to_ast(module_name, tree)

    def _resolve_module(self, module_name: str) -> Path:
        for search_path in self.search_paths:
            possible_module_path = search_path / module_name
            if possible_module_path.exists():
                return possible_module_path

        raise RuntimeError(
            f"Module '{module_name}' was not found "
            f"(search paths: {self.search_paths})."
        )

    def _compile_to_ast(self, module_name: str, tree: Tree) -> None:
        module = _compile_tree_to_ast(module_name, tree, self.modules)
        self.modules[module_name] = module

    def _generate_objects(self) -> None:
        # TODO: builds could be parallelized here, does it bring any benefit?
        for module_name, module in self.modules.items():
            self._generate_module_object(module_name, module)

    def _generate_module_object(self, module_name: str, module: Module) -> None:
        llvm_ir = module.get_llvm_ir()

        llvm_ir_path = self.build_dir / f"{module_name}.ll"
        llvm_ir_path.write_text(llvm_ir)

        object_path = self.build_dir / f"{module_name}.o"
        _compile_to_object_code(llvm_ir_path, object_path)

        self.modules[module_name] = module
        self.object_paths.append(object_path)


def _read_entrypoint(entrypoint: Path) -> str:
    # Module
    if entrypoint.is_dir():
        return _read_module(entrypoint)

    if entrypoint.suffix != _FILE_EXT:
        raise ValueError(f"'{entrypoint}' is not an alto source file")

    return _read_text(entrypoint)


def _read_module(module_path: Path) -> str:
    return _FILE_SEP.join(
        [
            f"# file: {file.name}\n{_read_text(file)}"
            for file in module_path.glob(f"*{_FILE_EXT}")
        ]
    )


def _read_text(file: Path) -> str:
    # Add trailing newline to avoid special cases in the parser
    return f"{file.read_text().strip()}\n"


def compile_to_ast(
    module_name: str,
    code: str,
    dependencies: Optional[dict[str, Module]] = None,
) -> Module:
    tree = parse(code)
    return _compile_tree_to_ast(module_name, tree, dependencies or {})


def _compile_tree_to_ast(
    module_name: str,
    tree: Tree,
    dependencies: Optional[dict[str, Module]] = None,
) -> Module:
    ast = build_module_ast(module_name, tree, dependencies or {})
    ast = optimize_ast(ast)
    return ast


def _compile_to_object_code(llvm_ir_path: Path, object_path: Path) -> None:
    try:
        subprocess.run(
            [
                "llc",
                # Create object files instead of assembly (the default)
                "--filetype=obj",
                # Enable use of the ptr type
                # https://llvm.org/docs/OpaquePointers.html#opaque-pointers-mode
                "--opaque-pointers",
                str(llvm_ir_path),
                "-o",
                str(object_path),
                # Optimize build
                "-O3",
            ],
            check=True,
        )
    except subprocess.SubprocessError:
        pass


def _link(object_paths: list[Path], exe_path: Path) -> None:
    alto_module_dir = Path(__file__).parent
    clibs_dir = Path(__file__).parent / "clibs"
    # ctr1, crti, crt0 are a specially crafted object file
    # that initialize the libc and call the main function
    ctr1_obj_path = clibs_dir / "crt1.o"
    ctri_obj_path = clibs_dir / "crti.o"
    ctrn_obj_path = clibs_dir / "crtn.o"
    # libc provides the functions to perform system calls
    libc_obj_path = clibs_dir / "libc.a"
    # libgc provides the allocation and garbage collection
    libgc_obj_path = clibs_dir / "libgc.a"
    try:
        subprocess.run(
            [
                "ld.lld",
                *map(str, object_paths),
                str(ctr1_obj_path),
                str(ctri_obj_path),
                str(ctrn_obj_path),
                str(libc_obj_path),
                str(libgc_obj_path),
                "-o",
                str(exe_path),
            ],
            check=True,
        )
    except subprocess.SubprocessError:
        pass
