from lark import Lark, Tree
from lark.indenter import Indenter

# Always match following newlines (start matches the initial ones)
# At the end of blocks, a newline is not necessary
_GRAMMAR = r"""
%import common.WS_INLINE
%ignore WS_INLINE
COMMENT: /#[^\n]*/
%ignore /\s*/ COMMENT

%declare _INDENT _DEDENT

_NL: /(\r?\n *)+/
_LEFT_PAR: "("
_RIGHT_PAR: ")"
_LEFT_ANG_PAR: "<"
_LEFT_ANG_PAR_MARK: "!<"
_RIGHT_ANG_PAR: ">"

NAME: /(?!true|false|fun|import|return|if|for|let)[a-zA-Z_][a-zA-Z0-9_]*/
PATH.1: /[a-zA-Z_][a-zA-Z0-9_\/]*/
INT: /(-\s*)?[0-9][0-9_]*(u8|u16|u32|u64|i8|i16|i32|i64)?/
// Priority 1 to match before INT
FLOAT.1: /(-\s*)?[0-9][0-9_]*\.[0-9_]*(f32|f64)?/
BOOL: "true" | "false"
STR: /".*?(?<!\\)"/
SCALAR: "bool" 
    | "u8"
    | "u16"
    | "u32"
    | "u64"
    | "i8"
    | "i16"
    | "i32"
    | "i64"
    | "f32"
    | "f64"

start: _NL* _toplevel

_toplevel: (import | const | struct_def | fun_dec | fun_def)*
import: "import" PATH _NL*
const: "const" NAME type "=" literal _NL*

struct_def: "struct" NAME generic_params? ":" _NL+ _INDENT struct_fields _NL? methods _NL? _DEDENT 
generic_params: _LEFT_ANG_PAR (NAME ",")* NAME? _RIGHT_ANG_PAR
struct_fields: (struct_field _NL)* struct_field?
struct_field: NAME type
methods: method*
method: _method_def ":" _NL block _NL*

fun_dec: _fun_sign_dec ";" _NL*
fun_def: _fun_sign_def ":" _NL block _NL*
_fun_sign_dec: "fun" NAME _LEFT_PAR fun_args _RIGHT_PAR _INDENT? type _DEDENT?
_fun_sign_def: "fun" NAME generic_params? _LEFT_PAR fun_args _RIGHT_PAR _INDENT? type _DEDENT?
// TODO: accept generic params here?
_method_def: "fun" NAME _LEFT_PAR self_arg fun_args _RIGHT_PAR _INDENT? type _DEDENT?
fun_args: (fun_arg "," _NL?)* fun_arg?
fun_arg: NAME? type
self_arg: "self"

block: _INDENT _stmt* _DEDENT

// STATEMENTS
_stmt: return
    | if_else
    | expr
    | assignment
    | _for

return: "return" _expr _NL

if_else: _if _after_if?
_if: "if" _expr ":" _NL block
_after_if: _else_if | _else
_else_if: "else" if_else
_else: "else" ":" _NL block

_for: for_in | for_condition
for_in: "for" NAME "in" _expr ":" _NL block
for_condition: "for" _expr? ":" _NL block

assignment: (var_assignment | _expr_memory) "=" _expr _NL
var_assignment: (LET MUT?)? NAME
LET: "let"
MUT: "mut"

expr: _expr _NL

// EXPRESSIONS
_expr: _op
_expr_no_op: literal
    | identifier 
    | _expr_memory
    | generic_instance 
    | call 
    | _group
_expr_memory: access | index
_group: _LEFT_PAR _expr _RIGHT_PAR

// Lowest priority operators first
_op: _logical

_logical: not | and | or | _cmp
not: "not" _cmp
and: _cmp "and" _logical
or: _cmp "or" _logical

_cmp: lt | lte | gt | gte | eq | uneq | _sum
lt: _sum "<" _cmp
lte: _sum "<=" _cmp
gt: _sum ">" _cmp
gte: _sum ">=" _cmp
eq: _sum "==" _cmp
uneq: _sum "!=" _cmp

_sum: add | sub | _prod
add: _prod "+" _sum
sub: _prod "-" _sum

_prod: mul | div | rem | _term
mul: _term "*" _prod
div: _term "/" _prod
rem: _term "%" _prod

_term: neg | _expr_no_op
neg: "-" _expr

call: _expr_no_op _LEFT_PAR _call_args _RIGHT_PAR
_call_args: (_expr "," _NL?)* _expr?

generic_instance: NAME _LEFT_ANG_PAR_MARK _generic_args _RIGHT_ANG_PAR
_generic_args: (_generic_arg "," _NL?)* _generic_arg?
_generic_arg: type

identifier: NAME
access: _expr_no_op "." NAME
index: _expr_no_op "[" _expr "]"
literal: INT | FLOAT | BOOL | STR

type: generic_instance | reference | NAME | SCALAR
reference: "ref" (generic_instance | NAME | SCALAR)
"""


class _WhiteSpaceIndenter(Indenter):
    NL_type = "_NL"
    OPEN_PAREN_types = ["_LEFT_PAR"]  # , "_LEFT_ANG_PAR"]
    CLOSE_PAREN_types = ["_RIGHT_PAR"]  # , "_RIGHT_ANG_PAR"]
    INDENT_type = "_INDENT"
    DEDENT_type = "_DEDENT"
    tab_len = 3


def parse(code: str) -> Tree:
    parser = Lark(
        _GRAMMAR,
        parser="lalr",
        postlex=_WhiteSpaceIndenter(),
    )
    return parser.parse(f"{code}\n")
