import subprocess
import sys
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import Optional

import click
import click_pathlib

from alto.builder import build_source


@click.group()
def main() -> None:
    pass


@main.command()
@click.argument(
    "file_or_folder",
    type=click_pathlib.Path(exists=True),
)
@click.option(
    "-s",
    "--search-path",
    type=click_pathlib.Path(exists=True),
    default=[],
    multiple=True,
    help="Path to use to search for modules.",
)
@click.option(
    "-o",
    "--output",
    type=click_pathlib.Path(exists=False),
    default=None,
    help="Path to the executable to build.",
)
def build(
    file_or_folder: Path,
    output: Optional[Path],
    search_path: list[Path],
):
    """Build executable from source.

    FILE_OR_FOLDER can point to a file or a module
    folder containing a main function.
    """
    executable = output or Path(file_or_folder.stem)
    try:
        build_source(file_or_folder, executable, search_path)
    except ValueError as err:
        raise click.ClickException(str(err))


@main.command(
    context_settings=dict(
        # Allow unknown arguments: we collect them and pass
        # them to the executable
        ignore_unknown_options=True,
        # Disable help to avoid confusion
        help_option_names=[],
    ),
)
@click.argument(
    "file_or_folder",
    type=click_pathlib.Path(exists=True),
)
@click.argument(
    "program_args",
    nargs=-1,
    type=click.UNPROCESSED,
)
def run(file_or_folder: Path, program_args: list[str]):
    """Build source to a temporary folder and run.

    FILE_OR_FOLDER can point to a file or a module
    folder containing a main function.

    All arguments and options passed after FILE_OR_FOLDER
    will be passed verbatim to the program.

    This command is helpful for scripts to emulate
    a scripting languages such as python, where you
    can just run code without worrying about compilation.
    """
    _run(file_or_folder, program_args)


def _run(file_or_folder: Path, program_args: list[str]) -> None:
    with TemporaryDirectory(prefix="alto-") as tmp_dir:
        executable = Path(tmp_dir) / file_or_folder.stem
        try:
            build_source(file_or_folder, executable, [])
        except ValueError as err:
            raise click.ClickException(str(err))

        proc = subprocess.run(
            [executable, *program_args],
            # Forwards all standard streams
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        sys.exit(proc.returncode)


if __name__ == "__main__":
    main()
