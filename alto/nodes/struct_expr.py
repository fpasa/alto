from __future__ import annotations

from dataclasses import dataclass, field, replace
from typing import Optional, Iterable

from alto.nodes.constants import Type
from alto.nodes.helpers import UniqueNumberGenerator, Reference
from alto.nodes.module import Struct, get_llvm_type, MethodDefinition
from alto.nodes.expressions import (
    Expression,
    use_builder,
    add_ref,
    Builder,
)


@dataclass
class StructConstruction(Expression):
    struct: Struct = field(compare=False)
    field_values: list[Expression]

    def get_type(self, hint: Optional[Type] = None) -> Type:
        return Reference(self.struct)

    def specialize(self, variant_map: dict[str, Type]) -> StructConstruction:
        return replace(
            self,
            struct=self.struct.specialize(variant_map),
            field_values=[f.specialize(variant_map) for f in self.field_values],
        )

    def get_fields_recursively(self) -> Iterable[tuple[list[int], str, Expression]]:
        for i, (field_type, field_value) in enumerate(
            zip(self.struct.fields.values(), self.field_values),
        ):
            # In case the field value is a struct construction,
            # we can optimize by allocating a single struct
            # and just directly assigning fields.
            #
            # This is not only an optimization, but is necessary
            # because otherwise we'd need special logic to
            # copy the source structure for the case where
            # the structure field is not a reference.
            is_struct_construction = isinstance(field_value, StructConstruction)
            is_reference = isinstance(field_type, Reference)
            if is_struct_construction and not is_reference:
                for j, ty, value in field_value.get_fields_recursively():  # type: ignore
                    yield [i, *j], ty, value
                continue

            yield [i], field_type, field_value

    def get_llvm_ir(
        self,
        ref_gen: UniqueNumberGenerator,
        type_hint: Optional[Type] = None,
    ) -> tuple[str, str]:
        ref_size = ref_gen.get_ref()
        ref = ref_gen.get_ref()
        ty = self.struct.qual_name

        # TODO: could be shared with struct write operation
        setters = []
        for indices, field_type, field_value in self.get_fields_recursively():
            indices_ir = ", ".join([f"i32 {i}" for i in indices])
            field_llvm_type = get_llvm_type(field_type)

            value, value_ir = field_value.get_llvm_ir(ref_gen, type_hint)
            setter_ref = ref_gen.get_ref()
            # We manually build the IR here to ensure the other of
            # references is correct. Using builder obscures the
            # order for this advanced case.
            ir = (
                f"{value_ir}\n"
                # First get address of field
                f"{setter_ref} = getelementptr inbounds %{ty}, ptr {ref}, i32 0, {indices_ir}\n"
                # Then set the field
                f"store {field_llvm_type} {value}, ptr {setter_ref}"
            )
            setters.append(ir.strip())
        construction_ir = "\n".join(setters)

        # TODO: look for ways to reduce duplication
        # For the builtins Array struct we cheat a little while
        # computing the struct size, to allocate memory for the array.
        if (
            self.struct.generic_parent is not None
            and self.struct.generic_parent.qual_name == "-builtins-.Array"
        ):
            # TODO: fix type here for structs
            item_type = get_llvm_type(list(self.struct.variant_map.values())[0])
            return ref, (
                # Compute the size of the struct by starting from the null pointer
                # pretending there is an array of structure values
                # and getting the offset to the beginning of the second item.
                # https://stackoverflow.com/questions/14608250/how-can-i-find-the-size-of-a-type
                # -----
                # Also note that we use a dummy [0 x item_type] array, even if the length
                # is different that 0, just to convince LLVM to calculate the array size for us.
                # -----
                # At this point `value` must be the reference to the array size,
                # due to the field initialization loop above, and be of type u64.
                f"{ref_size} = getelementptr {{%{ty}, [0 x {item_type}]}}, ptr null, i32 0, i32 1, i64 {value}\n"
                f"{ref} = call ptr @GC_malloc(ptr {ref_size})\n"
                f"{construction_ir}"
            )

        return ref, (
            # Compute the size of the struct by starting from the null pointer
            # pretending there is an array of structure values
            # and getting the offset to the beginning of the second item.
            # https://stackoverflow.com/questions/14608250/how-can-i-find-the-size-of-a-type
            f"{ref_size} = getelementptr %{ty}, ptr null, i32 1\n"
            f"{ref} = call ptr @GC_malloc(ptr {ref_size})\n"
            f"{construction_ir}"
        )


@dataclass
class StructFieldReadOperation(Expression):
    helper: StructFieldAccessHelper

    def __str__(self):
        return (
            f"<StructFieldReadOperation {self.helper.struct.name}.{self.helper.field}>"
        )

    def get_type(self, hint: Optional[Type] = None) -> Type:
        return self.helper.get_type()

    def specialize(self, variant_map: dict[str, Type]) -> StructFieldReadOperation:
        return StructFieldReadOperation(self.helper.specialize(variant_map))

    @use_builder
    def get_llvm_ir(
        self,
        ref_gen: UniqueNumberGenerator,
        builder: Builder,
        type_hint: Optional[Type] = None,
    ) -> tuple[str, str]:
        field_type = self.get_type()
        ptr = builder.get_expr_llvm_ir(self.helper)

        # If the type of the field is an embedded struct,
        # we do not need a "load" operation to get
        # the pointer value
        if isinstance(field_type, Struct):
            return ptr, ""

        ref = ref_gen.get_ref()
        field_llvm_type = self.get_llvm_type()
        return ref, f"{ref} = load {field_llvm_type}, ptr {ptr}"


@dataclass
class StructFieldAccessHelper(Expression):
    """Never used directly anywhere, but used to share
    logic between struct field read and write operations.
    """

    object: Expression
    struct: Struct = field(compare=False)
    field: str = field()

    def get_type(self, hint: Optional[Type] = None) -> Type:
        return self.struct.fields[self.field]

    def specialize(self, variant_map: dict[str, Type]) -> StructFieldAccessHelper:
        return replace(
            self,
            object=self.object.specialize(variant_map),
            struct=self.struct.specialize(variant_map),
        )

    @use_builder
    @add_ref
    def get_llvm_ir(
        self,
        ref_gen: UniqueNumberGenerator,
        builder: Builder,
        type_hint: Optional[Type] = None,
    ) -> str:
        struct_ty = self.struct.qual_name
        # TODO: For nested struct access, we can optimize the loading
        #       by using a single getelementptr operation. Right now,
        #       a sequence of getelementptr, each with a single index,
        #       will be produced.
        value = builder.get_expr_llvm_ir(self.object, type_hint=type_hint)
        field_index = list(self.struct.fields.keys()).index(self.field)
        return f"getelementptr inbounds %{struct_ty}, ptr {value}, i32 0, i32 {field_index}"


@dataclass
class StructMethodAccessOperation(Expression):
    object: Expression
    struct: Struct = field(compare=False)
    method_name: str = field()

    @property
    def method(self) -> MethodDefinition:
        return self.struct.methods[self.method_name]

    def get_type(self, hint: Optional[Type] = None) -> Type:
        # TODO: function pointers
        return "DUMMY_TYPE"

    def specialize(self, variant_map: dict[str, Type]) -> StructMethodAccessOperation:
        return replace(self, object=self.object.specialize(variant_map))

    @use_builder
    @add_ref
    def get_llvm_ir(
        self,
        _ref_gen: UniqueNumberGenerator,
        builder: Builder,
        type_hint: Optional[Type] = None,
    ) -> str:
        field_type = self.get_llvm_type()
        ptr = builder.get_expr_llvm_ir(self.object)
        return f"load {field_type}, ptr {ptr}"
