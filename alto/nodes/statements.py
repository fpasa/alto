from __future__ import annotations

import abc
from dataclasses import dataclass, replace, field
from typing import Optional

from alto.nodes.constants import Type
from alto.nodes.helpers import specialize_type
from alto.nodes.module import (
    UniqueNumberGenerator,
    FunctionDefinition,
    Block,
)
from alto.nodes.expressions import (
    Expression,
    Builder,
    use_builder,
    Variable,
)
from alto.nodes.operators import Not
from alto.nodes.array_expr import ArrayItemAccessHelper
from alto.nodes.struct_expr import StructFieldAccessHelper


class Statement(abc.ABC):
    @abc.abstractmethod
    def specialize(self, variant_map: dict[str, Type]) -> Statement:
        """Specialize generic statement with the given type map."""


@dataclass
class ExpressionStatement(Statement):
    expr: Expression

    def specialize(self, variant_map: dict[str, Type]) -> ExpressionStatement:
        return ExpressionStatement(self.expr.specialize(variant_map))

    def get_llvm_ir(self, ref_gen: UniqueNumberGenerator) -> str:
        return self.expr.get_llvm_ir(ref_gen)[1]


@dataclass
class ReturnStatement(Statement):
    value: Expression
    function_return_type: str

    def specialize(self, variant_map: dict[str, Type]) -> ReturnStatement:
        return ReturnStatement(
            self.value.specialize(variant_map),
            specialize_type(self.function_return_type, variant_map),
        )

    @use_builder
    def get_llvm_ir(self, ref_gen: UniqueNumberGenerator, builder: Builder) -> str:
        value = builder.get_expr_llvm_ir(
            self.value, type_hint=self.function_return_type
        )
        ty = self.value.get_llvm_type(self.function_return_type)
        return f"ret {ty} {value}"


@dataclass
class VariableAssignment(Statement):
    variable: str
    value: Expression
    definition: bool = False
    mutable: bool = False

    def get_variable(self) -> Variable:
        ty = self.value.get_type()
        return Variable(self.variable, ty, mutable=self.mutable)

    def specialize(self, variant_map: dict[str, Type]) -> VariableAssignment:
        return replace(self, value=self.value.specialize(variant_map))

    @use_builder
    def get_llvm_ir(self, ref_gen: UniqueNumberGenerator, builder: Builder) -> str:
        ref = f"%{self.variable}"
        value = builder.get_expr_llvm_ir(self.value)
        llvm_type = self.value.get_llvm_type()

        store = f"store {llvm_type} {value}, ptr {ref}"
        if self.definition:
            return f"{ref} = alloca {llvm_type}\n{store}"

        return store


@dataclass
class StructFieldWriteOperation(Statement):
    helper: StructFieldAccessHelper
    value: Expression

    def specialize(self, variant_map: dict[str, Type]) -> StructFieldWriteOperation:
        return StructFieldWriteOperation(
            self.helper.specialize(variant_map),
            self.value.specialize(variant_map),
        )

    @use_builder
    def get_llvm_ir(self, ref_gen: UniqueNumberGenerator, builder: Builder) -> str:
        field_type = self.helper.get_type()
        field_llvm_type = self.helper.get_llvm_type()
        ptr = builder.get_expr_llvm_ir(self.helper)
        value = builder.get_expr_llvm_ir(self.value, type_hint=field_type)
        return f"store {field_llvm_type} {value}, ptr {ptr}"


@dataclass
class ArrayItemWriteOperation(Statement):
    helper: ArrayItemAccessHelper
    value: Expression

    def specialize(self, variant_map: dict[str, Type]) -> ArrayItemWriteOperation:
        return ArrayItemWriteOperation(
            self.helper.specialize(variant_map),
            self.value.specialize(variant_map),
        )

    @use_builder
    def get_llvm_ir(self, ref_gen: UniqueNumberGenerator, builder: Builder) -> str:
        field_type = self.helper.get_type()
        field_llvm_type = self.helper.get_llvm_type()
        ptr = builder.get_expr_llvm_ir(self.helper)
        value = builder.get_expr_llvm_ir(self.value, type_hint=field_type)
        return f"store {field_llvm_type} {value}, ptr {ptr}"


@dataclass
class IfStatement(Statement):
    function: FunctionDefinition = field(compare=False)
    condition: Expression
    if_blocks: list[Block] = field(default_factory=list)
    else_blocks: list[Block] = field(default_factory=list)
    after_block: Block = None
    num: int = 0

    @property
    def if_(self) -> Block:
        return self.if_blocks[-1]

    @property
    def else_(self) -> Block:
        return self.else_blocks[-1]

    def __post_init__(self) -> None:
        self.num = self.function.unique_gen.get_num()
        if_block = Block(self.function, f"if{self.num}")
        self.if_blocks.append(if_block)

    def add_else_if(self, condition: Expression) -> IfStatement:
        else_block = self.add_else()
        return else_block.add_if(condition, self.else_blocks)

    def add_else(self) -> Block:
        else_block = Block(self.function, f"else{self.num}")
        self.else_blocks.append(else_block)
        return else_block

    def specialize(self, variant_map: dict[str, Type]) -> IfStatement:
        return replace(
            self,
            condition=self.condition.specialize(variant_map),
            if_blocks=[b.specialize(variant_map) for b in self.if_blocks],
            else_blocks=[b.specialize(variant_map) for b in self.else_blocks],
            after_block=self.after_block.specialize(variant_map),
        )

    @use_builder
    def get_llvm_ir(self, ref_gen: UniqueNumberGenerator, builder: Builder) -> str:
        condition = builder.get_expr_llvm_ir(self.condition)
        if_block_name = self.if_blocks[0].name
        if self.else_blocks:
            otherwise_blocks_name = self.else_blocks[0].name
        else:
            otherwise_blocks_name = self.after_block.name
        return (
            f"br i1 {condition}, label %{if_block_name}, label %{otherwise_blocks_name}"
        )


@dataclass
class ForConditionStatement(Statement):
    function: FunctionDefinition = field(compare=False)
    condition: Optional[Expression] = None
    blocks: list[Block] = field(default_factory=list)
    after_block: Block = None
    num: int = 0

    def __post_init__(self) -> None:
        block = Block(self.function, f"for{self.num}")
        self.blocks.append(block)

        # Implement condition by simply adding an if with a "break"
        # that checks if the condition is respected
        if self.condition is not None:
            if_stmt = block.add_if(Not(self.condition), self.blocks)
            # This is a "break" statement in practice
            if_stmt.if_.add_statement(Goto(self.after_block.name))

    def specialize(self, variant_map: dict[str, Type]) -> ForConditionStatement:
        return replace(
            self,
            condition=self.condition and self.condition.specialize(variant_map),
            blocks=[block.specialize(variant_map) for block in self.blocks],
        )

    @use_builder
    def get_llvm_ir(self, ref_gen: UniqueNumberGenerator, builder: Builder) -> str:
        label = self.blocks[0].name
        return f"br label %{label}"


@dataclass
class Goto(Statement):
    """Unconditional jump used internally."""

    label: str

    def specialize(self, _type_map: dict[str, Type]) -> Goto:
        return self

    def get_llvm_ir(self, ref_gen: UniqueNumberGenerator) -> str:
        return f"br label %{self.label}"
