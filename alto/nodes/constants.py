from __future__ import annotations

from typing import Union, Literal

Type = Union[str, "Struct", "Reference"]
SIGNED_TYPES = {"i8", "i16", "i32", "i64", "isize"}
UNSIGNED_TYPES = {"u8", "u16", "u32", "u64", "usize"}
FLOAT_TYPES = {"f32", "f64"}
SCALAR_TYPES = {
    "bool",
    *UNSIGNED_TYPES,
    *SIGNED_TYPES,
    *FLOAT_TYPES,
}


Operator = Literal[
    "and", "or", "<", "<=", ">", ">=", "==", "!=", "+", "-", "*", "/", "%"
]
OPERATOR_NODE_TYPES = {
    "and",
    "or",
    "lt",
    "lte",
    "gt",
    "gte",
    "eq",
    "uneq",
    "add",
    "sub",
    "mul",
    "div",
    "rem",
}
SIGNED_OP_LLVM_MAP = {
    "+": "add",
    "-": "sub",
    "*": "mul",
    "/": "sdiv",
    "%": "srem",
    ">": "icmp sgt",
    ">=": "icmp sge",
    "<": "icmp slt",
    "<=": "icmp sle",
    "==": "icmp eq",
    "!=": "icmp ne",
}
UNSIGNED_OP_LLVM_MAP = {
    "+": "add",
    "-": "sub",
    "*": "mul",
    "/": "udiv",
    "%": "urem",
    ">": "icmp ugt",
    ">=": "icmp uge",
    "<": "icmp ult",
    "<=": "icmp ule",
    "==": "icmp eq",
    "!=": "icmp ne",
}
FLOAT_OP_LLVM_MAP = {
    "+": "fadd",
    "-": "fsub",
    "*": "fmul",
    "/": "fdiv",
    "%": "frem",
    ">": "icmp ogt",
    ">=": "icmp oge",
    "<": "icmp olt",
    "<=": "icmp ole",
    "==": "icmp oeq",
    "!=": "icmp one",
}
BOOL_OP_LLVM_MAP = {
    "=": "icmp eq",
    "!=": "icmp ne",
    "and": "and",
    "or": "or",
}
