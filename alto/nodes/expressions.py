from __future__ import annotations

import abc
from dataclasses import dataclass, replace, field
from functools import wraps
from typing import Optional, Literal as TypingLiteral, overload

from alto.nodes.module import Function, get_llvm_type, Struct, reference_structs
from alto.nodes.constants import (
    Type,
    SIGNED_TYPES,
    UNSIGNED_TYPES,
    FLOAT_TYPES,
)
from alto.nodes.helpers import specialize_type, UniqueNumberGenerator


@dataclass
class Builder:
    ref_gen: UniqueNumberGenerator = field(compare=False)
    expr_ir: list[str] = field(default_factory=list)

    def get_expr_llvm_ir(
        self,
        expr: Expression,
        *,
        type_hint: Optional[Type] = None,
    ) -> str:
        ref, ir = expr.get_llvm_ir(self.ref_gen, type_hint=type_hint)
        self.expr_ir.append(ir)
        return ref

    # We need to support both expressions and statements
    @overload
    def gen_ir(self, base: str) -> str:
        ...

    @overload
    def gen_ir(self, base: tuple[str, str]) -> tuple[str, str]:
        ...

    def gen_ir(self, base):
        expr_ir = "\n".join(ir for ir in self.expr_ir if ir)

        if isinstance(base, str):
            return f"{expr_ir}\n{base}".strip()

        ref, base = base
        return ref, f"{expr_ir}\n{base}".strip()


def use_builder(func):
    """Keep tracks of expression references and IR to simplify
    generating IR with many sub-expressions.
    """

    @wraps(func)
    def wrapper(
        self, ref_gen: UniqueNumberGenerator, *args, **kwargs
    ) -> tuple[str, str]:
        builder = Builder(ref_gen)
        base = func(self, ref_gen, builder, *args, **kwargs)
        return builder.gen_ir(base)

    return wrapper


def add_ref(func):
    """This is to shorten a bit the common generation of
    references and wrapping.
    """

    @wraps(func)
    def wrapper(
        self, ref_gen: UniqueNumberGenerator, *args, **kwargs
    ) -> tuple[str, str]:
        ir = func(self, ref_gen, *args, **kwargs)
        # The reference has to be generated after the child code
        # to be able to number references sequentially
        ref = ref_gen.get_ref()
        return ref, f"{ref} = {ir}"

    return wrapper


class Expression(abc.ABC):
    def get_llvm_type(self, hint: Optional[Type] = None) -> Type:
        return get_llvm_type(reference_structs(self.get_type(hint)))

    @abc.abstractmethod
    def get_type(self, hint: Optional[Type] = None) -> Type:
        """Return the type of the expression"""

    @abc.abstractmethod
    def specialize(self, variant_map: dict[str, Type]) -> Expression:
        """Specialize generic expression with the given type map."""

    @abc.abstractmethod
    def get_llvm_ir(
        self,
        ref_gen: UniqueNumberGenerator,
        type_hint: Optional[Type] = None,
    ) -> tuple[str, str]:
        """Return the reference name and LLVM IR for this expression."""


@dataclass
class NumberLiteral(Expression):
    value: str
    type: Type
    # Whether the literal had a specified type or not
    untyped: bool = True

    VALID_TYPES = set()

    def __repr__(self) -> str:
        return f"{self.value}{self.type}"

    def get_type(self, hint: Optional[Type] = None) -> Type:
        if self.untyped and hint in self.VALID_TYPES:
            return hint
        return self.type

    def specialize(self, variant_map: dict[str, Type]) -> NumberLiteral:
        return replace(self, type=specialize_type(self.type, variant_map))

    def get_llvm_ir(
        self,
        _ref_gen: UniqueNumberGenerator,
        type_hint: Optional[Type] = None,
    ) -> tuple[str, str]:
        return self.value, ""


@dataclass
class IntLiteral(NumberLiteral):
    type: Type = "i64"
    VALID_TYPES = {*SIGNED_TYPES, *UNSIGNED_TYPES}


@dataclass
class FloatLiteral(NumberLiteral):
    type: Type = "f64"
    VALID_TYPES = FLOAT_TYPES


@dataclass
class BoolLiteral(Expression):
    value: TypingLiteral["true", "false"]

    def __repr__(self) -> str:
        return self.value

    def get_type(self, _hint: Optional[Type] = None) -> Type:
        return "bool"

    def specialize(self, _type_map: dict[str, Type]) -> BoolLiteral:
        return self

    def get_llvm_ir(
        self,
        _ref_gen: UniqueNumberGenerator,
        type_hint: Optional[Type] = None,
    ) -> tuple[str, str]:
        value = "1" if self.value == "true" else "0"
        return value, ""


@dataclass
class StringLiteral(Expression):
    value: str
    type: Type  # Always set to str struct
    ref: Optional[str] = None

    def __repr__(self) -> str:
        return f'literal "{self.value}"'

    def get_type(self, _hint: Optional[Type] = None) -> Type:
        return self.type

    def specialize(self, _type_map: dict[str, Type]) -> StringLiteral:
        return self

    def get_toplevel_llvm_ir(self, ref_gen: UniqueNumberGenerator) -> str:
        self.ref = ref_gen.get_global_ref("string-")
        length = len(self.value)
        return (
            # Here, we do not use the type of the real str struct
            # because this is statically store in the constant section
            # of the memory. Later on, we use the pointer as a str struct
            # instance, because the memory layout is exaclty the same.
            f"{self.ref} = constant {{ i64, [{length+1} x i8] }} "
            f'{{ i64 {length}, [{length+1} x i8] c"{self.value}\\00" }}'
        )

    def get_llvm_ir(
        self,
        ref_gen: UniqueNumberGenerator,
        type_hint: Optional[Type] = None,
    ) -> tuple[str, str]:
        assert self.ref is not None, "self.ref cannot be set to None"
        return self.ref, ""


@dataclass
class Call(Expression):
    function: Function = field(compare=False)
    arg_values: list[Expression]

    def get_type(self, hint: Optional[Type] = None) -> Type:
        return self.function.return_type

    def specialize(self, variant_map: dict[str, Type]) -> Call:
        return Call(
            function=self.function.specialize(variant_map),
            arg_values=[a.specialize(variant_map) for a in self.arg_values],
        )

    @use_builder
    @add_ref
    def get_llvm_ir(
        self,
        _ref_gen: UniqueNumberGenerator,
        builder: Builder,
        type_hint: Optional[Type] = None,
    ) -> str:
        ty = self.get_llvm_type()
        fun_name = self.function.qual_name
        arguments = sep = ", ".join(
            self._get_arg_llvm_ir(value, self.function.get_arg_type(i), builder)
            for i, value in enumerate(self.arg_values)
        )

        return f"call {ty} @{fun_name}({arguments})"

    def _get_arg_llvm_ir(
        self, value: Expression, arg_type: Type, builder: Builder
    ) -> str:
        ty = value.get_llvm_type(arg_type)
        arg_ir = builder.get_expr_llvm_ir(value, type_hint=arg_type)
        return f"{ty} {arg_ir}"


@dataclass
class Argument(Expression):
    name: str
    type: Type = field(compare=False)

    def __str__(self) -> str:
        return f"argument '{self.name}' of type {self.type}"

    def get_ref(self) -> str:
        return f"%-arg_{self.name}-"

    def get_type(
        self,
        hint: Optional[Type] = None,
        type_hint: Optional[Type] = None,
    ) -> Type:
        return self.type

    def specialize(self, variant_map: dict[str, Type]) -> Argument:
        return replace(self, type=specialize_type(self.type, variant_map))

    def get_llvm_ir(
        self,
        _ref_gen: UniqueNumberGenerator,
        type_hint: Optional[Type] = None,
    ) -> tuple[str, str]:
        return self.get_ref(), ""


@dataclass
class Variable(Expression):
    name: str
    type: Type
    mutable: bool = False

    def get_type(
        self,
        hint: Optional[Type] = None,
        type_hint: Optional[Type] = None,
    ) -> Type:
        return reference_structs(self.type)

    def specialize(self, variant_map: dict[str, Type]) -> Variable:
        return replace(self, type=specialize_type(self.type, variant_map))

    def __str__(self) -> str:
        return str(self.type)

    def get_llvm_ir(
        self,
        ref_gen: UniqueNumberGenerator,
        type_hint: Optional[Type] = None,
    ) -> tuple[str, str]:
        ref = ref_gen.get_ref()
        ty = self.get_llvm_type(type_hint)
        return ref, f"{ref} = load {ty}, ptr %{self.name}"


@dataclass
class GenericInstance:
    """This class represent generic instantiation within
    other generic code.

    In the context of generic code, generic instantiation
    could still contain generic types, for instance in the example:

    struct Array<T>:
       fun iter(self) ArrayIterator!<T>:
          return ArrayIterator!<T>()

    struct ArrayIterator<T>:
       ...

    In this case we use this class to keep track of
    the instantiation and whenever .specialize() is called
    generate the appropriate code.

    In normal code, whenever a generic is used, we directly
    specialize the struct and not use this class.

    TODO: For now this only works for structs.
    """

    object: Struct
    generic_params: list[str] = field(default_factory=list)

    def __str__(self) -> str:
        params = "".join(self.generic_params)
        return f"{self.object.name}<{params}>"

    def specialize(self, variant_map: dict[str, Type]) -> Struct:
        return self.object.specialize(variant_map)
