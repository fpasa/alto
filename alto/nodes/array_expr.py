from __future__ import annotations

from dataclasses import dataclass
from typing import Optional

from alto.nodes.constants import Type
from alto.nodes.helpers import UniqueNumberGenerator, dereference_type
from alto.nodes.module import Struct
from alto.nodes.expressions import (
    Expression,
    use_builder,
    add_ref,
    Builder,
    GenericInstance,
)


def is_array(ty: Type) -> bool:
    if isinstance(ty, Struct) or isinstance(ty, GenericInstance):
        struct = ty if isinstance(ty, Struct) else ty.object
        return (
            struct.qual_name == "-builtins-.Array"
            or struct.generic_parent.qual_name == "-builtins-.Array"
        )

    return False


# TODO: check duplication with struct classes
@dataclass
class ArrayItemReadOperation(Expression):
    helper: ArrayItemAccessHelper

    def get_type(self, hint: Optional[Type] = None) -> Type:
        return self.helper.get_type()

    def specialize(self, variant_map: dict[str, Type]) -> ArrayItemReadOperation:
        return ArrayItemReadOperation(self.helper.specialize(variant_map))

    @use_builder
    @add_ref
    def get_llvm_ir(
        self,
        _ref_gen: UniqueNumberGenerator,
        builder: Builder,
        type_hint: Optional[Type] = None,
    ) -> str:
        field_type = self.get_llvm_type()
        ptr = builder.get_expr_llvm_ir(self.helper)
        return f"load {field_type}, ptr {ptr}"


@dataclass
class ArrayItemAccessHelper(Expression):
    """This expression type is never used directly anywhere,
    but used to share logic between array item read and write operations.
    """

    object: Expression
    index: Expression

    def get_type(self, hint: Optional[Type] = None) -> Type:
        object_type = dereference_type(self.object.get_type())
        # Right now we assume this one is an Array struct
        assert is_array(object_type), "Indexing only supports arrays as of now"

        # The type could also be a generic array such as Array<T>
        if isinstance(object_type, Struct):
            struct_type = object_type
        else:
            struct_type = object_type.object

        # TODO: fix type for structs here
        if not struct_type.variant_map:
            # In this case, the accessed item is still generic,
            # for instance because a generic Array!<T> is being
            # indexed.
            return struct_type.generic_params[0]

        return list(struct_type.variant_map.values())[0]

    def specialize(self, variant_map: dict[str, Type]) -> ArrayItemAccessHelper:
        return ArrayItemAccessHelper(
            self.object.specialize(variant_map),
            self.index.specialize(variant_map),
        )

    @use_builder
    @add_ref
    def get_llvm_ir(
        self,
        ref_gen: UniqueNumberGenerator,
        builder: Builder,
        type_hint: Optional[Type] = None,
    ) -> str:
        # TODO: fix type_hints
        # Assuming this is an Array (or a reference thereof),
        # which is a struct and therefore has a qual_name
        obj_type = dereference_type(self.object.get_type(type_hint)).qual_name
        item_type = self.get_llvm_type(type_hint)
        obj = builder.get_expr_llvm_ir(self.object, type_hint=type_hint)
        index = builder.get_expr_llvm_ir(self.index, type_hint="u64")
        # TODO: check array indexing bounds
        # Note that we use a dummy [0 x item_type] array, even if the length
        # is different that 0, just to convince LLVM to calculate the position for us.
        return (
            f"getelementptr {{%{obj_type}, [0 x {item_type}]}}, "
            f"ptr {obj}, i32 0, i32 1, i64 {index}"
        )
