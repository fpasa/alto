from __future__ import annotations

from dataclasses import dataclass, replace, make_dataclass, field
from typing import Optional

from alto.nodes.module import get_llvm_type
from alto.nodes.helpers import UniqueNumberGenerator
from alto.nodes.constants import (
    Type,
    SIGNED_TYPES,
    FLOAT_TYPES,
    UNSIGNED_TYPES,
    Operator,
    SIGNED_OP_LLVM_MAP,
    UNSIGNED_OP_LLVM_MAP,
    FLOAT_OP_LLVM_MAP,
    BOOL_OP_LLVM_MAP,
)
from alto.nodes.expressions import (
    Expression,
    use_builder,
    add_ref,
    Builder,
)


@dataclass
class Neg(Expression):
    value: Expression

    def get_type(self, hint: Optional[Type] = None) -> Type:
        return self.value.get_type(hint)

    def specialize(self, variant_map: dict[str, Type]) -> Neg:
        return replace(self, value=self.value.specialize(variant_map))

    @use_builder
    @add_ref
    def get_llvm_ir(
        self,
        _ref_gen: UniqueNumberGenerator,
        builder: Builder,
        type_hint: Optional[Type] = None,
    ) -> str:
        ty = self.get_type(type_hint)
        llvm_ty = self.get_llvm_type(type_hint)
        value = builder.get_expr_llvm_ir(self.value, type_hint=type_hint)
        if ty in SIGNED_TYPES:
            return f"sub {llvm_ty} 0, {value}"
        elif ty in FLOAT_TYPES:
            return f"fneg {llvm_ty} {value}"
        elif ty in UNSIGNED_TYPES:
            raise RuntimeError(f"Cannot negate unsigned value of type '{ty}'")
        elif ty == "bool":
            return f"sub {llvm_ty} 1, {value}"
        else:
            raise NotImplementedError(f"Negation not implemented for type '{ty}'")


@dataclass
class Not(Neg):
    ...


@dataclass
class BinaryOperation(Expression):
    a: Expression
    b: Expression
    operator: Operator

    @classmethod
    def alias(cls, name: str, operator: str):
        return make_dataclass(
            name,
            fields=[
                (
                    "operator",
                    str,
                    field(default=operator, repr=False),
                )
            ],
            bases=(cls,),
        )

    def get_type(self, hint: Optional[Type] = None) -> Type:
        if self.operator in {"+", "-", "*", "/", "%"}:
            return self.a.get_type(hint)
        return "bool"

    def specialize(self, variant_map: dict[str, Type]) -> BinaryOperation:
        return replace(
            self,
            a=self.a.specialize(variant_map),
            b=self.b.specialize(variant_map),
        )

    @use_builder
    @add_ref
    def get_llvm_ir(
        self,
        _ref_gen: UniqueNumberGenerator,
        builder: Builder,
        type_hint: Optional[Type] = None,
    ) -> str:
        ty_a = self.a.get_type(type_hint)
        ty_b = self.b.get_type(type_hint)

        # Ensure literals are converted if possible
        if type_hint is None:
            ty_a = self.a.get_type(ty_b)
            ty_b = self.a.get_type(ty_a)

        if ty_a != ty_b:
            raise RuntimeError(
                f"Operator '{self.operator}' requires the same type on both sides, "
                f"got '{ty_a}' and '{ty_b}' instead."
            )

        arg_ty = ty_a
        llvm_ty = get_llvm_type(arg_ty)
        a = builder.get_expr_llvm_ir(self.a, type_hint=type_hint)
        b = builder.get_expr_llvm_ir(self.b, type_hint=type_hint)

        if arg_ty in SIGNED_TYPES:
            if self.operator not in SIGNED_OP_LLVM_MAP:
                raise RuntimeError(
                    f"Operator '{self.operator}' not supported for type '{arg_ty}'."
                )
            op = SIGNED_OP_LLVM_MAP[self.operator]
        elif arg_ty in UNSIGNED_TYPES:
            if self.operator not in UNSIGNED_OP_LLVM_MAP:
                raise RuntimeError(
                    f"Operator '{self.operator}' not supported for type '{arg_ty}'."
                )
            op = UNSIGNED_OP_LLVM_MAP[self.operator]
        elif arg_ty in FLOAT_TYPES:
            if self.operator not in FLOAT_OP_LLVM_MAP:
                raise RuntimeError(
                    f"Operator '{self.operator}' not supported for type '{arg_ty}'."
                )
            op = FLOAT_OP_LLVM_MAP[self.operator]
        elif arg_ty == "bool":
            if self.operator not in BOOL_OP_LLVM_MAP:
                raise RuntimeError(
                    f"Operator '{self.operator}' not supported for type '{arg_ty}'."
                )
            op = BOOL_OP_LLVM_MAP[self.operator]
        else:
            raise NotImplementedError(
                f"Operator '{self.operator}' not implemented for type '{arg_ty}'"
            )

        return f"{op} {llvm_ty} {a}, {b}"


And = BinaryOperation.alias("And", "and")
Or = BinaryOperation.alias("And", "or")
LT = BinaryOperation.alias("LT", "<")
LTE = BinaryOperation.alias("LTE", "<=")
GT = BinaryOperation.alias("GT", ">")
GTE = BinaryOperation.alias("GTE", "<=")
Eq = BinaryOperation.alias("Eq", "==")
Uneq = BinaryOperation.alias("Uneq", "!=")
Add = BinaryOperation.alias("Add", "+")
Sub = BinaryOperation.alias("Sub", "-")
Mul = BinaryOperation.alias("Mul", "*")
Div = BinaryOperation.alias("Div", "/")
Rem = BinaryOperation.alias("Rem", "%")
