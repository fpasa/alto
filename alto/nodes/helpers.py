from __future__ import annotations

from dataclasses import dataclass
from typing import Optional, Union, Iterable

from .constants import Type


class UniqueNumberGenerator:
    def __init__(self):
        self.i = 0

    def get_num(self) -> int:
        temp = self.i
        self.i += 1
        return temp

    def peek_num(self) -> int:
        return self.i + 1

    def get_ref(self) -> str:
        return f"%{self.get_num()}"

    def get_global_ref(self, prefix: str = "") -> str:
        return f"@{prefix}{self.get_num()}"


def join_ir(items, sep: str = "\n\n", **kwargs) -> str:
    items = items.values() if isinstance(items, dict) else items
    return sep.join(item.get_llvm_ir(**kwargs) for item in items)


def specialize_type(ty: Type, variant_map: dict[str, Type]) -> Type:
    """This function specializes types for generics.

    It will take a mapping such as {"T": "u32"} and return:

        T -> u32
        MyStruct<T> -> MyStruct<u32>

    The struct case is the reason we can just use mapping[ty].
    """
    if isinstance(ty, str):
        return variant_map.get(ty, ty)

    if isinstance(ty, Reference):
        return Reference(specialize_type(ty.type, variant_map))

    # This must now be a Struct
    struct_variant_map = {t: specialize_type(t, variant_map) for t in ty.generic_params}
    return ty.specialize(struct_variant_map)


@dataclass
class Reference:
    type: Type

    def __str__(self):
        return f"reference to {self.type}"


def dereference_type(ty: Type) -> Type:
    """Unwraps the reference type and returns the type the reference
    points to.

    For non reference types, the function returns the type unchanged.
    """
    return ty.type if isinstance(ty, Reference) else ty
