from __future__ import annotations

import abc
import re
from dataclasses import dataclass, field, replace
from textwrap import indent
from typing import Optional, Iterable, Union, Callable

from alto.nodes.constants import Type
from alto.nodes.helpers import (
    UniqueNumberGenerator,
    join_ir,
    specialize_type,
    Reference,
)

UNNECESSARY_NEWLINES = re.compile(r"(\n\s*){3,}")


def get_llvm_type(
    ty: Type,
    struct_llvm_type: Callable[[Type], str] = lambda ty: "ptr",
) -> str:
    if isinstance(ty, str):
        return {
            "bool": "i1",
            "u8": "i8",
            "u16": "i16",
            "u32": "i32",
            "u64": "i64",
            # TODO: make this architecture dependant
            "usize": "i64",
            "isize": "i64",
        }.get(ty, ty)

    if isinstance(ty, Struct):
        return struct_llvm_type(ty)

    if isinstance(ty, Reference):
        return "ptr"

    return ty


def _get_type_name(ty: Type) -> str:
    if isinstance(ty, Struct):
        return ty.qual_name

    return ty


def reference_structs(ty: Type) -> Type:
    """If the type is a struct, return a reference to it,
    otherwise return the type unchanged.
    """
    return Reference(ty) if isinstance(ty, Struct) else ty


def _remove_unnecessary_newlines(value: str) -> str:
    return UNNECESSARY_NEWLINES.sub("\n\n", value).strip()


def _join_struct_ir(structs: Iterable["Struct"], module: Module) -> tuple[str, str]:
    body, methods = zip(*[s.get_llvm_ir(current_module=module) for s in structs])
    return "\n".join(body), "\n\n".join(methods)


@dataclass
class Module:
    name: str
    structs: dict[str, Struct] = field(default_factory=dict)
    functions: dict[str, Function] = field(default_factory=dict)
    string_literals: list["StringLiteral"] = field(default_factory=list)

    unique_gen: UniqueNumberGenerator = field(
        default_factory=UniqueNumberGenerator,
        compare=False,
        repr=False,
    )

    def add_function_dec(
        self,
        name: str,
        return_type: str,
        args: Optional[list[Type]] = None,
        c_fun: bool = False,
    ) -> FunctionDeclaration:
        module = None if c_fun else self
        fun = FunctionDeclaration(module, name, return_type, args or [])
        self.functions[name] = fun
        return fun

    def add_function_def(
        self,
        name: str,
        return_type: str,
        args: Optional[dict[str, Type]] = None,
        generic_params: Optional[list[str]] = None,
    ) -> FunctionDefinition:
        fun = FunctionDefinition(
            self,
            name,
            return_type,
            args or {},
            generic_params or [],
        )
        self.functions[name] = fun
        return fun

    def add_struct(
        self,
        name: str,
        fields: Optional[dict[str, Type]] = None,
        generic_params: Optional[list[str]] = None,
    ) -> Struct:
        struct = Struct(self, name, generic_params or [], fields or {})
        self.structs[name] = struct
        return struct

    def add_string_literal(self, value: str, ty: Struct) -> "StringLiteral":
        from alto.nodes.expressions import StringLiteral

        lit = StringLiteral(value, ty)
        self.string_literals.append(lit)
        return lit

    def __str__(self) -> str:
        return f"module '{self.name}'"

    def _get_structs_ir(self) -> str:
        if not self.structs:
            return ""

        joined_body, joined_methods = _join_struct_ir(self.structs.values(), self)
        return f"{joined_body}\n\n{joined_methods}"

    def get_llvm_ir(self) -> str:
        structs = self._get_structs_ir()
        string_literals = "\n".join(
            lit.get_toplevel_llvm_ir(self.unique_gen) for lit in self.string_literals
        )
        funs = join_ir(self.functions)
        return _remove_unnecessary_newlines(f"{structs}\n\n{string_literals}\n\n{funs}")


@dataclass
class Function(abc.ABC):
    module: Optional[Module] = field(compare=False)
    name: str
    return_type: Type

    @property
    def qual_name(self) -> str:
        """Name mangling is necessary to avoid conflicts when linking different modules."""
        # If module is not defined (i.e. when linking with C libraries), use name alone
        if self.module is None:
            return self.name
        # The function -main-.main needs to remain main for initialization
        if self.module.name == "-main-" and self.name == "main":
            return self.name
        return f"{self.module.name}.{self.name}"

    @abc.abstractmethod
    def get_arg(self, name_or_index: Union[str, int]) -> "Argument":
        """Get argument by index or name."""

    @abc.abstractmethod
    def get_args(self) -> list["Argument"]:
        """Get all arguments."""

    @abc.abstractmethod
    def get_arg_type(self, name_or_index: Union[str, int]) -> Type:
        """Get argument type by index or name."""

    def __str__(self) -> str:
        return f"function '{self.name}'"


@dataclass
class FunctionDeclaration(Function):
    arg_types: list[Type] = field(default_factory=list)

    def get_arg(self, name_or_index: Union[str, int]) -> "Argument":
        from alto.nodes.expressions import Argument

        assert isinstance(
            name_or_index, int
        ), f"Function declaration has no argument names, got '{name_or_index}'"

        ty = self.get_arg_type(name_or_index)
        return Argument(str(name_or_index), ty)

    def get_args(self) -> list["Argument"]:
        from alto.nodes.expressions import Argument

        return [Argument(str(i), ty) for i, ty in enumerate(self.arg_types)]

    def get_arg_type(self, name_or_index: Union[str, int]) -> Type:
        assert isinstance(
            name_or_index, int
        ), f"Function declaration has no argument names, got '{name_or_index}'"

        if name_or_index >= len(self.arg_types):
            raise RuntimeError(
                f"Argument '{name_or_index}' does not exist for {self}, "
                f"which only has {len(self.arg_types)} arguments"
            )

        return self.arg_types[name_or_index]

    def get_llvm_ir(self) -> str:
        ref_gen = UniqueNumberGenerator()
        return_type = get_llvm_type(self.return_type)
        args = ", ".join(get_llvm_type(arg_type) for arg_type in self.arg_types)
        return f"declare {return_type} @{self.qual_name}({args})"


@dataclass
class FunctionDefinition(Function):
    args: dict[str, Type] = field(default_factory=dict)
    generic_params: list[str] = field(default_factory=list)

    blocks: list[Block] = field(default_factory=lambda: [])
    unique_gen: UniqueNumberGenerator = field(
        default_factory=UniqueNumberGenerator,
        compare=False,
        repr=False,
    )

    generic_parent: Optional[FunctionDefinition] = field(default=None, compare=False)
    variants: list[FunctionDefinition] = field(default_factory=list, compare=False)
    variant_map: dict[str, Type] = field(default_factory=dict, compare=False)

    def __post_init__(self):
        # In the case of generic variants this might be filled already
        if not self.blocks:
            self.blocks.append(Block(self))

    def get_arg(self, name_or_index: Union[str, int]) -> "Argument":
        from alto.nodes.expressions import Argument

        name = self._get_arg_name(name_or_index)
        ty = self.args[name]
        return Argument(name, ty)

    def get_args(self) -> list["Argument"]:
        from alto.nodes.expressions import Argument

        return [Argument(name, ty) for name, ty in self.args.items()]

    def get_arg_type(self, name_or_index: Union[str, int]) -> Type:
        name = self._get_arg_name(name_or_index)
        return self.args[name]

    def _get_arg_name(self, name_or_index: Union[str, int]) -> str:
        if isinstance(name_or_index, str):
            if name_or_index not in self.args:
                raise RuntimeError(
                    f"Argument '{name_or_index}' does not exist for {self}"
                )

            return name_or_index

        if name_or_index >= len(self.args):
            raise RuntimeError(
                f"Argument '{name_or_index}' does not exist for {self}, "
                f"which only has {len(self.args)} arguments"
            )

        return list(self.args.keys())[name_or_index]

    def _blocks(self, blocks: Optional[list[Block]] = None) -> Iterable[Block]:
        from alto.nodes.statements import IfStatement, ForConditionStatement

        if blocks is None:
            blocks = self.blocks

        for block in blocks:
            yield block

            for stmt in block.statements:
                if isinstance(stmt, IfStatement):
                    yield from self._blocks(stmt.if_blocks)
                    yield from self._blocks(stmt.else_blocks)
                elif isinstance(stmt, ForConditionStatement):
                    yield from self._blocks(stmt.blocks)

    def specialize(self, variant_map: dict[str, Type]) -> FunctionDefinition:
        # Check if such variant already exists
        existing_variant = self._find_variant(variant_map)
        if existing_variant is not None:
            return existing_variant

        variant = replace(
            self,
            name=self._get_variant_name(variant_map),
            return_type=self._specialize_return_type(variant_map),
            args=self._specialize_args(variant_map),
            generic_params=[],
            blocks=[b.specialize(variant_map) for b in self.blocks],
            generic_parent=self,
            variant_map=variant_map,
        )

        self.variants.append(variant)
        return variant

    def _find_variant(
        self, variant_map: dict[str, Type]
    ) -> Optional[FunctionDefinition]:
        for variant in self.variants:
            if variant.variant_map == variant_map:
                return variant

        return None

    def _get_variant_name(self, variant_map: dict[str, Type]) -> str:
        types = "-".join(_get_type_name(ty) for ty in variant_map.values())
        return f"-{self.name}_{types}-"

    def _specialize_return_type(self, variant_map: dict[str, Type]) -> Type:
        return specialize_type(self.return_type, variant_map)

    def _specialize_args(self, variant_map: dict[str, Type]) -> dict[str, Type]:
        return {a: specialize_type(t, variant_map) for a, t in self.args.items()}

    def add_statement(self, stmt: "Statement") -> None:
        self.blocks[-1].add_statement(stmt)

    def add_return(self, value: "Expression") -> "ReturnStatement":
        return self.blocks[-1].add_return(value)

    def add_if(self, condition: "Expression") -> "IfStatement":
        return self.blocks[-1].add_if(condition, self.blocks)

    def to_declaration(self) -> FunctionDeclaration:
        return FunctionDeclaration(
            module=self.module,
            name=self.name,
            return_type=self.return_type,
            arg_types=list(self.args.values()),
        )

    def get_llvm_ir(self) -> str:
        # It can happen that a struct is specialized before
        # its methods are defined. This happens for instance for
        # the str struct which has a field specializing the Array
        # struct at struct definition time (before methods
        # are defined).
        #
        # Struct specialization causes the methods to be specialized
        # (e.g. copied), even if they contain no code block yet.
        #
        # If we suspect this could be the case, we specialize
        # the blocks again now, because we are sure that they
        # are defined at this point.
        if self.generic_parent is not None and not self.blocks:
            self.blocks = [
                b.specialize(self.variant_map) for b in self.generic_parent.blocks
            ]

        if self.generic_params:
            return join_ir(self.variants)

        ref_gen = UniqueNumberGenerator()
        return_type = get_llvm_type(self.return_type)
        args = ", ".join(
            f"{get_llvm_type(arg.type)} {arg.get_ref()}" for arg in self.get_args()
        )
        body = join_ir(self._blocks(), ref_gen=ref_gen)
        return f"define {return_type} @{self.qual_name}({args}) {{\n{body}\n}}"


@dataclass
class Method:
    struct: Struct = field(default=None, compare=False)

    @property
    def qual_name(self) -> str:
        return f"{self.struct.qual_name}.{self.name}"

    def __str__(self) -> str:
        return f"Struct method '{self.struct.name}.{self.name}'"

    # To avoid infinite recursion, do not specialize self argument
    def _specialize_args(self, variant_map: dict[str, Type]) -> dict[str, Type]:
        return {
            a: t if a == "self" else specialize_type(t, variant_map)
            for a, t in self.args.items()
        }


@dataclass
class MethodDeclaration(Method, FunctionDeclaration):
    ...


@dataclass
class MethodDefinition(Method, FunctionDefinition):
    struct: Struct = field(default=None, compare=False)

    def to_declaration(self) -> MethodDeclaration:
        return MethodDeclaration(
            module=self.module,
            name=self.name,
            return_type=self.return_type,
            arg_types=list(self.args.values()),
            struct=self.struct,
        )


@dataclass
class Struct:
    module: Module = field(compare=False)
    name: str
    generic_params: list[str] = field(default_factory=list)
    fields: dict[str, Type] = field(default_factory=dict)

    methods: dict[str, MethodDefinition] = field(default_factory=dict)
    generic_parent: Optional[Struct] = field(default=None, compare=False)
    variants: list[Struct] = field(default_factory=list, compare=False)

    variant_map: dict[str, Type] = field(default_factory=dict, compare=False)

    @property
    def qual_name(self) -> str:
        return f"{self.module.name}.{self.name}"

    def specialize(self, variant_map: dict[str, Type]) -> Struct:
        # Check if such variant already exists
        existing_variant = self._find_variant(variant_map)
        if existing_variant is not None:
            return existing_variant

        # Modify the fields that do not require further specialization
        # and add the resulting variant it to the variants list first,
        # so that generic structs depending on each other cyclically
        # do not cause infinite recursion.
        variant = replace(
            self,
            generic_params=[],
            generic_parent=self,
            variant_map=variant_map,
        )
        self.variants.append(variant)

        variant.name = self._get_variant_name(variant_map)
        variant.fields = self._specialize_fields(variant_map)
        variant.methods = self._specialize_methods(variant_map)

        return variant

    def _find_variant(self, variant_map: dict[str, Type]) -> Optional[Struct]:
        for variant in self.variants:
            if variant.variant_map == variant_map:
                return variant

        return None

    def _get_variant_name(self, variant_map: dict[str, Type]) -> str:
        types = "-".join(_get_type_name(ty) for ty in variant_map.values())
        return f"-{self.name}_{types}-"

    def _specialize_fields(self, variant_map: dict[str, Type]) -> dict[str, Type]:
        return {f: specialize_type(t, variant_map) for f, t in self.fields.items()}

    def _specialize_methods(
        self, variant_map: dict[str, Type]
    ) -> dict[str, MethodDefinition]:
        return {f: m.specialize(variant_map) for f, m in self.methods.items()}

    def __str__(self) -> str:
        return f"struct '{self.name}'"

    def add_method(
        self,
        name: str,
        return_type: str,
        args: Optional[dict[str, Type]] = None,
        generic_params: Optional[list[str]] = None,
    ) -> MethodDefinition:
        fun = MethodDefinition(
            self.module,
            name,
            return_type,
            args or {},
            struct=self,
            generic_params=generic_params or [],
        )
        self.methods[name] = fun
        return fun

    def get_llvm_type(self) -> str:
        types = ", ".join(
            get_llvm_type(ty, lambda ty: f"%{ty.qual_name}")
            for ty in self.fields.values()
        )
        return f"{{ {types} }}"

    def get_llvm_ir(self, current_module: Optional[Module] = None) -> tuple[str, str]:
        assert current_module is not None
        if self.generic_params:
            return _join_struct_ir(self.variants, current_module)

        llvm_type = self.get_llvm_type()

        methods = self.methods
        method_sep = "\n\n"
        if self.module != current_module:
            # If the struct is imported, we need just declarations of the methods
            methods = [m.to_declaration() for m in self.methods.values()]
            method_sep = "\n"
        methods_ir = join_ir(methods, sep=method_sep)

        return f"%{self.qual_name} = type {llvm_type}", methods_ir


@dataclass
class Block:
    function: FunctionDefinition = field(compare=False)
    name: str = "start"

    statements: list["Statement"] = field(default_factory=list)

    def add_statement(self, stmt: "Statement") -> None:
        self.statements.append(stmt)

    def add_return(self, value: "Expression") -> "ReturnStatement":
        from alto.nodes.statements import ReturnStatement

        stmt = ReturnStatement(value, self.function.return_type)
        self.statements.append(stmt)
        return stmt

    def add_if(self, condition: "Expression", blocks: list[Block]) -> "IfStatement":
        from alto.nodes.statements import IfStatement

        if_stmt = IfStatement(self.function, condition)
        self.add_statement(if_stmt)

        after_block = Block(self.function, f"after{if_stmt.num}")
        blocks.append(after_block)

        if_stmt.after_block = after_block

        return if_stmt

    def add_for_condition(
        self,
        blocks: list[Block],
        condition: Optional["Expression"] = None,
    ) -> "ForConditionStatement":
        from alto.nodes.statements import ForConditionStatement

        num = self.function.unique_gen.get_num()

        after_block = Block(self.function, f"after{num}")
        blocks.append(after_block)

        for_stmt = ForConditionStatement(
            self.function,
            condition,
            after_block=after_block,
            num=num,
        )
        self.add_statement(for_stmt)

        return for_stmt

    def specialize(self, variant_map: dict[str, Type]) -> Block:
        return replace(
            self,
            statements=[s.specialize(variant_map) for s in self.statements],
        )

    def get_llvm_ir(self, ref_gen: UniqueNumberGenerator) -> str:
        statements = join_ir(self.statements, sep="\n", ref_gen=ref_gen)
        ind_statements = indent(statements, "  ")

        if not self.name:
            return ind_statements

        return f"{self.name}:\n{ind_statements}"
