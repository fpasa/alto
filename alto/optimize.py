from __future__ import annotations

from dataclasses import fields, is_dataclass
from typing import Callable, Any as TypingAny, TypeVar

from alto.nodes import *
from alto.nodes import Statement
from alto.nodes.struct_expr import StructConstruction

T = TypeVar("T")
Parents = list[tuple[TypingAny, str]]


def _get_list(seq: list[T], i: int) -> Optional[T]:
    if i < len(seq):
        return seq[i]
    return None


def _is_terminator(stmt: Statement) -> bool:
    return isinstance(stmt, (ReturnStatement, IfStatement, Goto))


def optimize_ast(module: Module) -> Module:
    """This function applies clean-up, syntactic sugar
    and optimizations to the AST.
    """
    module.parent = None
    return (
        Transformer()
        .transform((Call,), _import_functions)
        .transform((Struct,), _import_structs)
        .transform((IfStatement,), _goto_after_blocks)
        .transform((ForConditionStatement,), _add_looping)
        .transform((Goto, Block), EmptyBlockRemover())
        # TODO: add optimization to remove if statements when the
        #       if statement contains a single goto. In this case
        #       the intermediate indirection can be eliminated.
        # ---
        # TODO: add optimization to avoid too much aliasing of
        #       LLVM variables. That could avoid unnecessary alloca
        #       and store operations. Also, loads could be reused.
        .build(module)
    )


class Transformer:
    def __init__(self):
        self.transformers = []

    def transform(self, node_types, transformer: Callable) -> Transformer:
        self.transformers.append((node_types, transformer))
        return self

    def build(self, module: Module) -> Module:
        for node, parents in reversed(list(self._breadth_first(module))):
            for node_types, transformer in self.transformers:
                if isinstance(node, node_types):
                    transformer(node, parents)
        return module

    def _breadth_first(
        self,
        obj,
        parents: Optional[Parents] = None,
        visited: set = None,
    ) -> Iterable:
        parents = parents or []
        visited = visited or set()

        yield obj, parents

        for f in fields(obj):
            if not f.compare:
                continue

            child = getattr(obj, f.name)
            if id(child) in visited:
                continue

            visited.add(id(child))

            if is_dataclass(child):
                yield from self._breadth_first(
                    child,
                    [*parents, (obj, f.name)],
                    visited,
                )
            elif isinstance(child, dict):
                for key, item in child.items():
                    if is_dataclass(item):
                        yield from self._breadth_first(
                            item,
                            [*parents, (obj, f.name)],
                            visited,
                        )
            elif isinstance(child, list):
                for item in child:
                    if is_dataclass(item):
                        yield from self._breadth_first(
                            item,
                            [*parents, (obj, f.name)],
                            visited,
                        )


def _import_functions(call: Call, parents: Parents) -> None:
    """Functions imported from modules need to be declared."""
    function = call.function
    if isinstance(function, Method):
        return
    module, _ = parents[0]
    if function.module.name != module.name:
        fun_dec = FunctionDeclaration(
            function.module,
            function.name,
            function.return_type,
            [arg.type for arg in function.get_args()],
        )
        module.functions[fun_dec.qual_name] = fun_dec


def _import_structs(struct: Struct, parents: Parents) -> None:
    """Structs imported from modules need to be copied."""
    if isinstance(struct, GenericInstance):
        return

    module, _ = parents[0]
    if struct.module.name == module.name:
        return

    # Always import topmost struct
    if struct.generic_parent is not None:
        struct = struct.generic_parent

    # Import struct
    module.structs[struct.qual_name] = struct

    # Also import structs that this struct depends on
    # because they are used as fields types.
    for field_type in struct.fields.values():
        is_struct = isinstance(field_type, Struct)
        is_struct_ref = isinstance(field_type, Reference) and isinstance(
            field_type.type, Struct
        )
        if is_struct:
            struct_dep = field_type
        elif is_struct_ref:
            struct_dep = field_type.type
        else:
            continue

        # Always import topmost struct
        if struct_dep.generic_parent is not None:
            struct_dep = struct_dep.generic_parent

        module.structs[struct_dep.qual_name] = struct_dep


def _goto_after_blocks(if_stmt: IfStatement, parents: Parents) -> None:
    block, _ = parents[-1]
    parent, field_name = parents[-2]

    blocks: list[Block] = getattr(parent, field_name)
    index = blocks.index(block)
    after_block = _get_list(blocks, index + 1)

    if after_block is None:
        return

    # Remove after block if empty
    if not after_block.statements:
        blocks.remove(after_block)
        return

    if not (if_stmt.if_.statements and _is_terminator(if_stmt.if_.statements[-1])):
        if_stmt.if_.add_statement(Goto(after_block.name))

    if if_stmt.else_blocks:
        if not (
            if_stmt.else_.statements and _is_terminator(if_stmt.else_.statements[-1])
        ):
            if_stmt.else_.add_statement(Goto(after_block.name))


def _add_looping(for_stmt: ForConditionStatement, _parents: Parents) -> None:
    """This transform actually implements looping by adding gotos
    from the end of the loop to the beginning of it.
    """
    first_block = for_stmt.blocks[0]
    last_block = for_stmt.blocks[-1]

    # Add looping behavior
    last_block.add_statement(Goto(first_block.name))


class EmptyBlockRemover:
    def __init__(self):
        self._referenced_blocks = set()

    def __call__(self, obj: Union[Goto, Block], parents: Parents) -> None:
        """Remove empty unreferenced blocks.

        This transform relies on ordering of the tree and assumes
        that all gotos are processed before the block.
        """
        if isinstance(obj, Goto):
            self._referenced_blocks.add(obj.label)
        elif not obj.statements and obj.name not in self._referenced_blocks:
            parent, fname = parents[-1]
            container = getattr(parent, fname)
            if isinstance(container, list):
                container.remove(obj)
