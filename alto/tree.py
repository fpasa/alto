from __future__ import annotations

from collections import defaultdict
from typing import Any, Callable

from lark import Tree, Token

from alto.nodes import *
from alto.nodes import (
    Type,
    Expression,
    StructFieldAccessHelper,
    ArrayItemAccessHelper,
    Not,
)
from alto.nodes.array_expr import ArrayItemReadOperation, is_array
from alto.nodes.constants import OPERATOR_NODE_TYPES, SCALAR_TYPES
from alto.nodes.helpers import Reference, dereference_type
from alto.nodes.operators import (
    Neg,
    And,
    Or,
    LT,
    LTE,
    GT,
    GTE,
    Eq,
    Uneq,
    Add,
    Sub,
    Mul,
    Div,
    Rem,
)
from alto.nodes.struct_expr import (
    StructConstruction,
    StructFieldReadOperation,
    StructMethodAccessOperation,
)

Node = Union[Tree, Token]

UNARY_OP_MAP = {
    "not": Not,
    "neg": Neg,
}
BINARY_OP_MAP = {
    "and": And,
    "or": Or,
    "lt": LT,
    "lte": LTE,
    "gt": GT,
    "gte": GTE,
    "eq": Eq,
    "uneq": Uneq,
    "add": Add,
    "sub": Sub,
    "mul": Mul,
    "div": Div,
    "rem": Rem,
}

BUILTINS = {"Array", "str"}


@dataclass
class Scope:
    parent: Optional[Scope] = None
    names: dict[str, Any] = field(default_factory=dict)

    def specialize(self) -> Scope:
        return Scope(self, names={**self.names})


@dataclass
class Context:
    module: Module
    scope: Scope = field(default_factory=Scope)

    def specialize(self) -> Context:
        return replace(self, scope=self.scope.specialize())


def build_module_ast(
    module_name: str,
    tree: Tree,
    dependencies: dict[str, Module],
) -> Module:
    module = Module(module_name)
    context = Context(module)

    # By default, some built-in symbols are added to each module
    _add_builtins(module, context, dependencies)

    # First pass: initialize definitions and declarations
    # so that following steps do not depend on order
    _initialize_module(tree, module, context, dependencies)

    # Second pass: build AST
    _build_module(tree, module, context)

    return module


def _initialize_module(
    tree: Tree,
    module: Module,
    context: Context,
    dependencies: dict[str, Module],
) -> None:
    # The order in the source should not have any effect on
    # code executions. For this reason this function needs
    # to declare and define objects in the right way.

    # To start, copy imports and struct definitions.
    # We need to do struct definitions first because function
    # arguments might depend on them.
    #
    # We also need to keep track of dependencies among
    # structures to be able to initialize them in the right order.
    struct_def_nodes = {}
    # struct -> dependencies (using dicts because sets are not ordered)
    struct_dependencies = defaultdict(dict)
    for node in tree.children:
        if is_of_type(node, "import"):
            dep_name = node.children[0].value
            # Only copy dependencies imported by module (not other modules)
            context.scope.names[dep_name] = dependencies[dep_name]
        elif is_of_type(node, "struct_def"):
            # Here we just want to "declare" the structure,
            # so we pass an empty list of fields and do not evaluate
            # any type annotation yet.
            name = node.children[0].value
            struct_def_nodes[name] = node

            struct_ctx, generic_params = _build_generic_params(
                context, node.children[1]
            )

            struct = module.add_struct(name, {}, generic_params)
            context.scope.names[name] = struct

            fields_of_struct_type = _extract_fields_of_struct_type(node)
            for struct_dep in fields_of_struct_type:
                struct_dependencies[struct_dep][name] = None

            if not fields_of_struct_type:
                struct_dependencies[None][name] = None

    struct_dependencies = {
        struct: list(dep.keys()) for struct, dep in struct_dependencies.items()
    }
    struct_order = list(order_dependencies(struct_dependencies))
    # Reorder structs
    module.structs = {
        struct_name: module.structs[struct_name] for struct_name in struct_order
    }

    # As a second step, do function declarations and definitions.
    # Moreover, add fields to and methods to structures, as
    # methods are basically functions, so they have the same
    # problem that their type annotations might depend on other
    # structures.
    #
    # Struct fields have a similar issue that they might depend on
    # another struct or itself, so they can only be evaluated later.
    for node in tree.children:
        # TODO: reduce code duplication
        if is_of_type(node, "fun_dec"):
            name = node.children[0].value
            args = [
                _resolve_type_reference(context, arg.children[1])
                for arg in node.children[1].children
            ]
            return_type = _resolve_type_reference(context, node.children[2])

            fun = module.add_function_dec(name, return_type, args)
            context.scope.names[name] = fun
        elif is_of_type(node, "fun_def"):
            name = node.children[0].value

            fun_ctx, generic_params = _build_generic_params(context, node.children[1])

            args = _extract_name_type_as_dict(
                fun_ctx,
                node.children[-3].children,
                resolve_func=_resolve_type_reference,
            )
            return_type = _resolve_type_reference(fun_ctx, node.children[-2])

            fun = module.add_function_def(name, return_type, args, generic_params)
            context.scope.names[name] = fun

    for struct_name in struct_order:
        node = struct_def_nodes[struct_name]
        struct = module.structs[struct_name]

        struct_ctx, generic_params = _build_generic_params(context, node.children[1])

        field_nodes = node.children[-1].children
        method_nodes = []
        if is_of_type(node.children[-1], "methods"):
            field_nodes = node.children[-2].children
            method_nodes = node.children[-1].children

        fields = _extract_name_type_as_dict(struct_ctx, field_nodes)
        struct.fields = fields

        for method_node in method_nodes:
            method_name = method_node.children[0].value

            args = _extract_name_type_as_dict(
                struct_ctx,
                method_node.children[2].children,
                resolve_func=_resolve_type_reference,
            )
            # Add self argument
            args_with_self = {"self": Reference(struct), **args}
            return_type = _resolve_type_reference(struct_ctx, method_node.children[3])

            method = struct.add_method(
                method_name, return_type, args_with_self, generic_params
            )


def _build_generic_params(context: Context, params: Tree) -> tuple[Context, list[str]]:
    ctx = context
    generic_params = []
    if is_of_type(params, "generic_params"):
        # This is a generic function
        generic_params = [p.value for p in params.children]

        ctx = context.specialize()
        for param in generic_params:
            ctx.scope.names[param] = param

    return ctx, generic_params


def _build_module(tree: Tree, module: Module, context: Context) -> None:
    for node in tree.children:
        if is_of_type(node, "fun_def"):
            name = node.children[0].value
            fun = module.functions[name]
            body = node.children[-1]

            spec_ctx = context.specialize()
            for arg in fun.get_args():
                spec_ctx.scope.names[arg.name] = arg
            _build_function_ast(spec_ctx, fun, body)
        elif is_of_type(node, "import") or is_of_type(node, "fun_dec"):
            pass
        elif is_of_type(node, "struct_def"):
            struct_name = node.children[0].value
            struct = module.structs[struct_name]

            method_nodes = []
            if is_of_type(node.children[-1], "methods"):
                method_nodes = node.children[-1].children

            for method_node in method_nodes:
                method_name = method_node.children[0].value
                method = struct.methods[method_name]
                body = method_node.children[-1]

                spec_ctx = context.specialize()
                for arg in method.get_args():
                    spec_ctx.scope.names[arg.name] = arg
                _build_function_ast(spec_ctx, method, body)
        else:
            raise NotImplementedError(f"Top-Level '{node.data}' not implemented")


def _add_builtins(
    module: Module,
    context: Context,
    dependencies: dict[str, Module],
) -> None:
    # Always add the std/builtins module to scope,
    # but do not add to the -builtins- module itself
    # to avoid recursion
    if module.name != "-builtins-" and "-builtins-" in dependencies:
        context.scope.names["-builtins-"] = dependencies["-builtins-"]

    # Add declaration of GC_malloc for allocations
    module.add_function_dec("GC_malloc", "ptr", args=["i64"], c_fun=True)


def is_of_type(node: Node, ty: Union[str, set[str]]) -> bool:
    ty = ty if isinstance(ty, set) else {ty}
    if isinstance(node, Tree):
        return node.data in ty
    return node.type in ty


def _resolve_type(ctx: Context, ty: Tree) -> Type:
    """Resolve the given tree to a type."""
    child = ty.children[0]
    if isinstance(child, Tree):
        if is_of_type(child, "reference"):
            return Reference(_resolve_type(ctx, child))
        elif is_of_type(child, "generic_instance"):
            return _resolve_generic_instance(ctx, child)
        else:
            raise NotImplementedError(f"Type '{child}' not implemented")

    str_ty = ty.children[0].value
    if str_ty in SCALAR_TYPES:
        return str_ty

    if str_ty == "str":
        return _resolve_identifier(ctx, "str")

    return _resolve_identifier(ctx, str_ty)


def _resolve_type_reference(ctx: Context, ty: Tree) -> Type:
    """Similar to the function above, but converts structs to
    structs references.
    """
    return reference_structs(_resolve_type(ctx, ty))


def _extract_name_type_as_dict(
    ctx: Context,
    args: list[Tree],
    resolve_func: Callable[[Context, Tree], Type] = _resolve_type,
) -> dict[str, Type]:
    return {arg.children[0].value: resolve_func(ctx, arg.children[1]) for arg in args}


def _build_function_ast(ctx: Context, fun: FunctionDefinition, body: Tree) -> None:
    _build_block_ast(ctx, fun.blocks, body)


def _build_block_ast(
    ctx: Context,
    blocks: list[Block],
    body: Tree,
) -> None:
    for stmt in body.children:
        _build_stmt_ast(ctx, blocks, blocks[-1], stmt)


def _build_stmt_ast(
    ctx: Context,
    blocks: list[Block],
    block: Block,
    stmt: Tree,
) -> None:
    if is_of_type(stmt, "return"):
        value = _build_expr_ast(ctx, stmt.children[0])
        block.add_return(value)

    elif is_of_type(stmt, "expr"):
        value = _build_expr_ast(ctx, stmt.children[0])
        block.add_statement(ExpressionStatement(value))

    elif is_of_type(stmt, "assignment"):
        obj = stmt.children[0]
        value = _build_expr_ast(ctx, stmt.children[1])
        if is_of_type(obj, "var_assignment"):
            _assign_variable(ctx, block, obj, value)
        elif is_of_type(obj, "access"):
            access_op = _build_expr_ast(ctx, obj)
            struct_field_write = StructFieldWriteOperation(access_op.helper, value)
            block.add_statement(struct_field_write)
        elif is_of_type(obj, "index"):
            access_op = _build_expr_ast(ctx, obj)
            array_item_write = ArrayItemWriteOperation(access_op.helper, value)
            block.add_statement(array_item_write)
        else:
            raise NotImplementedError(f"Assignment for {stmt}")

    elif is_of_type(stmt, "if_else"):
        condition = _build_expr_ast(ctx, stmt.children[0])
        if_stmt = block.add_if(condition, blocks)
        _build_block_ast(ctx, if_stmt.if_blocks, stmt.children[1])

        # There's an else statement
        if len(stmt.children) == 3:
            else_content = stmt.children[2]
            if_stmt.add_else()
            if is_of_type(else_content, "if_else"):
                _build_stmt_ast(
                    ctx,
                    if_stmt.else_blocks,
                    if_stmt.else_,
                    else_content,
                )
            else:
                _build_block_ast(ctx, if_stmt.else_blocks, else_content)

    elif is_of_type(stmt, "for_in"):
        # TODO: performance optimization for arrays.
        #       Since arrays are common, we should add some
        #       special case to avoid extra function calls.
        variable: str = stmt.children[0].value
        iterable = _build_expr_ast(ctx, stmt.children[1])

        # Before the loop, get the iterator, and make its variable
        # name unique so that it does not conflict with anything.
        num = block.function.unique_gen.peek_num()
        iterator = _var_object_method_call(
            ctx, f"-iterable_{num}-", iterable, "iter", mutable=True
        )
        block.add_statement(iterator)

        for_stmt = block.add_for_condition(blocks)

        # Before the body of the loop, get next item and assign it
        # to a variable as specified by the for ... in ... loop.
        item = _var_object_method_call(ctx, variable, iterator.get_variable(), "next")
        for_stmt.blocks[0].add_statement(item)

        _build_block_ast(ctx, for_stmt.blocks, stmt.children[-1])

    elif is_of_type(stmt, "for_condition"):
        condition = None
        if len(stmt.children) == 2:
            condition = _build_expr_ast(ctx, stmt.children[0])

        for_stmt = block.add_for_condition(blocks, condition)
        _build_block_ast(ctx, for_stmt.blocks, stmt.children[1])

    else:
        raise NotImplementedError(f"Statement '{stmt.data}' not implemented")


def _var_object_method_call(
    ctx: Context,
    variable: str,
    obj: Expression,
    method_name: str,
    mutable: bool = False,
) -> VariableAssignment:
    """Calls a method on an object and assigns the result to a variable"""
    method_access = StructMethodAccessOperation(
        obj,
        dereference_type(obj.get_type()),
        method_name,
    )
    call = Call(method_access.method, [obj])
    assignment = VariableAssignment(variable, call, definition=True, mutable=mutable)
    ctx.scope.names[variable] = assignment.get_variable()
    return assignment


def _assign_variable(ctx: Context, block: Block, obj: Tree, value: Expression) -> None:
    definition = is_of_type(obj.children[0], "LET")
    mutable = len(obj.children) > 1 and is_of_type(obj.children[1], "MUT")
    name: str = obj.children[-1].value

    symbol = ctx.scope.names.get(name)
    is_defined = symbol is not None
    if is_defined and definition:
        raise RuntimeError(f"Cannot redefine '{name}'")

    is_mutable_var = isinstance(symbol, Variable) and symbol.mutable
    if is_defined and not is_mutable_var:
        raise RuntimeError(f"Cannot assign to immutable variable '{name}'")

    if not is_defined and not definition:
        raise RuntimeError(f"Assignment to uninitialized variable '{name}'")

    assignment = VariableAssignment(
        name,
        value,
        definition=definition,
        # Propagate mutability
        mutable=mutable if definition else is_mutable_var,
    )

    ctx.scope.names[name] = assignment.get_variable()
    block.add_statement(assignment)


def _build_expr_ast(ctx: Context, expr: Tree, children=None) -> Any:
    if is_of_type(expr, "literal"):
        return _build_literal(ctx, expr.children[0])
    elif is_of_type(expr, "call"):
        callable = _build_expr_ast(ctx, expr.children[0])
        if not isinstance(
            callable,
            (
                Function,
                Struct,
                StructMethodAccessOperation,
                GenericInstance,
            ),
        ):
            raise RuntimeError(f"{callable} is not callable")

        parameters = [_build_expr_ast(ctx, e) for e in expr.children[1:]]

        if isinstance(callable, GenericInstance):
            # TODO: this right now only supports structs,
            #       but in theory also function calls should
            #       be allowed.
            return StructConstruction(callable, parameters)

        if isinstance(callable, Struct):
            if len(parameters) != len(callable.fields):
                raise RuntimeError(
                    f"{callable} has {len(callable.fields)}"
                    f"fields but {len(parameters)} were given"
                )

            return StructConstruction(callable, parameters)

        if isinstance(callable, StructMethodAccessOperation):
            access_op = callable
            callable = access_op.method
            # Add self parameter
            parameters = [access_op.object, *parameters]

        if len(parameters) != len(callable.get_args()):
            raise RuntimeError(
                f"{callable} has {len(callable.get_args())} "
                f"arguments but {len(parameters)} were given"
            )

        return Call(callable, parameters)
    elif is_of_type(expr, "identifier"):
        return _resolve_identifier(ctx, expr.children[0])
    elif is_of_type(expr, "access"):
        obj = _build_expr_ast(ctx, expr.children[0])
        attr = expr.children[1].value
        return _access_object(ctx, obj, attr)
    elif is_of_type(expr, "index"):
        obj = _build_expr_ast(ctx, expr.children[0])
        index = _build_expr_ast(ctx, expr.children[1])
        return _index_object(ctx, obj, index)
    elif is_of_type(expr, {"not", "neg"}):
        value = _build_expr_ast(ctx, expr.children[0])
        return UNARY_OP_MAP[expr.data](value)
    elif is_of_type(expr, OPERATOR_NODE_TYPES):
        a = _build_expr_ast(ctx, expr.children[0])
        b = _build_expr_ast(ctx, expr.children[1])
        return BINARY_OP_MAP[expr.data](a, b)
    elif is_of_type(expr, "generic_instance"):
        return _resolve_generic_instance(ctx, expr)
    else:
        raise NotImplementedError(f"Expression '{expr.data}' not implemented")


def _build_literal(ctx: Context, lit: Token) -> Expression:
    if is_of_type(lit, "INT"):
        value = lit.value
        ty = "i64"  # default integer literal type
        untyped = True

        if "i" in value:
            value, ty = value.split("i")
            ty = f"i{ty}"
            untyped = False
        elif "u" in value:
            value, ty = value.split("u")
            ty = f"u{ty}"
            untyped = False

        # Remove optional underscore separators
        value = value.replace("_", "")

        return IntLiteral(value, ty, untyped)
    elif is_of_type(lit, "FLOAT"):
        value = lit.value
        ty = "f64"  # default float literal type
        untyped = True

        if "f" in value:
            value, ty = value.split("f")
            ty = f"f{ty}"
            untyped = False

        # Remove optional underscore separators
        value = value.replace("_", "")

        return FloatLiteral(value, ty, untyped)
    elif is_of_type(lit, "BOOL"):
        return BoolLiteral(lit.value)
    elif is_of_type(lit, "STR"):
        # Get the str structure from the builtins module
        string_struct = ctx.scope.names["-builtins-"].structs["str"]
        # Index with [1:-1] to remove the quotes
        return ctx.module.add_string_literal(lit.value[1:-1], string_struct)
    else:
        raise NotImplementedError(f"Literal '{lit.type}' not implemented")


def _resolve_identifier(ctx: Context, identifier: str) -> Any:
    # Special built-in cases, but don't do this for the -builtins-
    # module itself, otherwise we got recursion
    if ctx.module.name != "-builtins-" and identifier in BUILTINS:
        builtins = ctx.scope.names["-builtins-"]
        return _access_object(ctx, builtins, identifier)

    if identifier not in ctx.scope.names:
        raise RuntimeError(f"Undefined identifier '{identifier}'")

    return ctx.scope.names[identifier]


def _access_object(ctx: Context, obj: Any, attr: str) -> Any:
    try:
        if isinstance(obj, Module):
            assert attr in obj.functions or attr in obj.structs
            return obj.functions.get(attr, obj.structs.get(attr))

        ty = dereference_type(obj.get_type())
        # TODO: make GenericInstance mimik the struct interface?
        if isinstance(ty, GenericInstance):
            ty = ty.object

        if isinstance(ty, Struct):
            assert attr in ty.fields or attr in ty.methods

            if attr in ty.methods:
                return StructMethodAccessOperation(obj, ty, attr)

            helper = StructFieldAccessHelper(obj, ty, attr)
            return StructFieldReadOperation(helper)
    except AssertionError:
        # Assertions cause the runtime error below to be raised
        pass

    # This happens when access for an obj type is not supported
    # but also when the object does not have the attribute
    # (checked with asserts above).
    raise RuntimeError(f"{obj} has no attribute '{attr}'")


def _index_object(_ctx: Context, obj: Any, index: Expression) -> Any:
    ty = dereference_type(obj.get_type())
    if is_array(ty):
        helper = ArrayItemAccessHelper(obj, index)
        return ArrayItemReadOperation(helper)

    raise RuntimeError(f"{obj} of type {ty} does not support indexing")


def _resolve_generic_instance(ctx: Context, instance: Tree) -> Any:
    """TODO"""
    generic_obj = _resolve_identifier(ctx, instance.children[0])

    params = []
    still_generic = False
    for expr, generic_param in zip(instance.children[1:], generic_obj.generic_params):
        try:
            param = _resolve_type(ctx, expr)
            params.append(param)

            if param == generic_param:
                still_generic = True
        except RuntimeError:
            # In generic code, it can happen that one of the
            # params is still a generic type. In this case,
            # we defer specialization to the final call with
            # fully resolved types.
            still_generic = True
            params.append(expr.children[0].value)

    if still_generic:
        return GenericInstance(generic_obj, params)

    if len(params) != len(generic_obj.generic_params):
        raise RuntimeError(
            f"{generic_obj} has {len(generic_obj.generic_params)}"
            f"generic parameters but {len(params)} were given"
        )

    variant_map = dict(zip(generic_obj.generic_params, params))
    return generic_obj.specialize(variant_map)


def _extract_fields_of_struct_type(struct: Tree) -> set[str]:
    """Extract from a struct all fields of struct type,
    and return the names of the used structures.

    For instance, running on:

    struct A:
        id u64
        other B
        yet_another C!<i32>
        last ref D

    will return [B, C, D].
    """
    generic_params = (
        {p.value for p in struct.children[1].children}
        if len(struct.children) == 4
        # Struct is not generic
        else {}
    )

    field_nodes = struct.children[-1].children
    if is_of_type(struct.children[-1], "methods"):
        field_nodes = struct.children[-2].children

    fields_of_struct_type = set()
    for field in field_nodes:
        field_type = _extract_struct_name(field.children[1])
        if field_type in SCALAR_TYPES or field_type in generic_params:
            continue
        fields_of_struct_type.add(field_type)

    return fields_of_struct_type


def _extract_struct_name(type_node: Union[Tree, str]) -> str:
    """Extract the name of the struct from a Tree which might
    be a generic instance, a reference or simply a struct name.
    """
    # This is necessary due to recursion in the case of references.
    if isinstance(type_node, str):
        return type_node

    ty = type_node.children[0]
    if is_of_type(ty, "generic_instance"):
        ty = ty.children[0]
    if is_of_type(ty, "reference"):
        return _extract_struct_name(ty.children[0])

    return ty.value


def order_dependencies(dependency_map: dict[Optional[str], list[str]]) -> Iterable[str]:
    """Returns items depending on each other ordered so that the items
    without dependencies come first.

    The function takes as input a dictionary of dependency -> dependants
    such as this one (None used to indicate absence of dependecy):

    {
        None: ["A", "B"],
        "A": ["C", "E"],  # Meaning C and D depend on A
        "C": ["D"],
    }

    The function will return [A, B, C, E, D]
    """
    if not dependency_map:
        return

    processed = set()
    queue = dependency_map[None]
    while queue:
        item = queue.pop(0)
        if item in processed:
            continue

        yield item

        queue.extend(dependency_map.get(item, ()))

        processed.add(item)
